const reporter = require('cucumber-html-reporter');

const options = {
    theme: 'bootstrap',
    jsonFile: 'test-reports/cucumber/cucumber-report.json',
    output: 'test-reports/cucumber/cucumber-html-report.html',
    reportSuiteAsScenarios: true,
    launchReport: false,
    metadata: {
        'App Version': '0.0.1', // Update with your app version
        'Test Environment': 'STAGING', // Update with your test environment
        Browser: 'Chrome',
        Platform: 'macOS',
        Parallel: 'Scenarios',
        Executed: 'Remote',
    },
};

reporter.generate(options);
