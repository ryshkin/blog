FROM node:lts-alpine

# Install MySQL client
RUN apk --no-cache add mysql-client

# Set the working directory to /app
WORKDIR /app

## Copy package.json and package-lock.json to the working directory
COPY package*.json ./

## Install app dependencies
RUN npm install

## Copy the rest of the application code to the working directory
COPY . .

## Change working directory to the proxy subfolder
WORKDIR /app/client

## Install client dependencies
RUN npm install --force

## Expose port 8080 for the Vue app
EXPOSE 8080

## Expose port 3000 for the ExpressJS app in the proxy folder
EXPOSE 3000

EXPOSE 1080

#WORKDIR ..
## Run all application
WORKDIR /app
CMD ["./start"]
