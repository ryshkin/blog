import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Login from '@/components/LoginUser'
import Logout from '@/components/LogoutUser'
import Register from '@/components/RegisterUser'
import Profile from '@/components/ProfileUser'
import ForgotPassword from '@/components/ForgotPassword'
import ResetPassword from '@/components/ResetPassword'
import PostAdd from '@/components/PostAdd'
import PostDetail from '@/components/PostDetail'
import NotFound from '@/components/NotFound'
import store from '@/store'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
    beforeEnter: (to, from, next) => {
      if (!store.state.isAuthenticated) {
        store.commit('setErrorMessage', 'You must be logged in first!')
        next({ name: 'login', query: { showError: true } })
      } else {
        next()
      }
    }
  },
  {
    path: '/add-post',
    name: 'PostAdd',
    component: PostAdd,
    beforeEnter: (to, from, next) => {
      if (!store.state.isAuthenticated) {
        store.commit('setErrorMessage', 'You must be logged in first!')
        next({ name: 'login', query: { showError: true } })
      } else {
        next()
      }
    }
  },
  {
    path: '/show-post/:id',
    name: 'PostDetail',
    component: PostDetail,
    beforeEnter: (to, from, next) => {
      if (!store.state.isAuthenticated) {
        store.commit('setErrorMessage', 'You must be logged in first!')
        next({ name: 'login', query: { showError: true } })
      } else {
        next()
      }
    }
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/forgot-password',
    name: 'forgotPassword',
    component: ForgotPassword
  },
  {
    path: '/reset-password/:token',
    name: 'resetPassword',
    component: ResetPassword
  },
  {
    path: '/logout',
    name: 'logout',
    component: Logout
  },
  {
    path: '/register',
    name: 'register',
    component: Register
  },
  {
    path: '/profile',
    name: 'profile',
    component: Profile,
    beforeEnter: (to, from, next) => {
      if (!store.state.isAuthenticated) {
        store.commit('setErrorMessage', 'You must be logged in first!')
        next({ name: 'login', query: { showError: true } })
      } else {
        next()
      }
    }
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: function () {
      return import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
    }
  },
  { path: '/:pathMatch(.*)*', name: 'not-found', component: NotFound },
  // if you omit the last `*`, the `/` character in params will be encoded when resolving or pushing
  { path: '/:pathMatch(.*)', name: 'bad-not-found', component: NotFound }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
