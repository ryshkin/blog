import { createStore } from 'vuex'
import config from './config'

const env = process.env.NODE_ENV || 'dev'
const store = createStore({
  state: {
    config: config[env],
    env: env,
    authToken: localStorage.getItem('authToken'),
    isAuthenticated: localStorage.getItem('authToken') !== null,
    errorMessage: '',
    successMessage: ''
  },
  mutations: {
    setErrorMessage (state, message) {
      state.errorMessage = message
    },
    clearErrorMessage (state) {
      state.errorMessage = ''
    },
    setSuccessMessage (state, message) {
      state.successMessage = message
    },
    clearSuccessMessage (state) {
      state.successMessage = ''
    },
    setAuthToken (state, token) {
      state.authToken = token
      state.isAuthenticated = token !== null
      localStorage.setItem('authToken', token)
    },
    clearAuthToken (state) {
      state.authToken = null
      state.isAuthenticated = false
      localStorage.removeItem('authToken')
    }
  }
})

export default store
