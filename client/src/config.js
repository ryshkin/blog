const config = {
  development: {
    apiBaseUrl: 'http://localhost:3000/api'
  },
  test: {
    apiBaseUrl: 'http://localhost:3000/api'
  },
  production: {
    apiBaseUrl: 'http://localhost:3000/api'
  }
}

module.exports = config
