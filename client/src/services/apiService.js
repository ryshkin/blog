import axios from 'axios'
import store from '@/store'
import router from '@/router'

const apiClient = axios.create({
  baseURL: store.state.config.apiBaseUrl,
  headers: {
    'Content-Type': 'application/json'
  }
})

apiClient.interceptors.request.use((config) => {
  const token = store.state.authToken
  if (token) {
    config.headers.Authorization = `Bearer ${token}`
  }
  return config
}, (error) => {
  return Promise.reject(error)
})

export default {
  async register (username, email, password, confirmPassword) {
    try {
      const response = await apiClient.post('/user/register', {
        username: username,
        email: email,
        password: password,
        confirm_password: confirmPassword
      })
      store.commit('setSuccessMessage', 'Congrats! Your registration has been successful.')
      await router.push({ name: 'home' })
      return response.data
    } catch (error) {
      store.commit('setErrorMessage', error)
      throw error.response.data
    }
  },
  async login (username, password) {
    try {
      const response = await apiClient.post('/user/login', {
        username,
        password
      })
      await store.commit('setAuthToken', response.data.token)
      await store.commit('clearErrorMessage')
      await router.push({ name: 'home' })
      return response.data
    } catch (error) {
      store.commit('setErrorMessage', 'Invalid username/password, Try again!')
      throw error.response ? error.response.data : error
    }
  },
  async logout () {
    try {
      const response = await apiClient.get('/user/logout')
      store.commit('clearAuthToken')
      await router.replace({ name: 'login' })
      return response.data
    } catch (error) {
      store.commit('setErrorMessage', error)
      if (error.response && error.response.status === 401) {
        store.commit('clearAuthToken')
        return true
      } else {
        throw error.response ? error.response.data : error
      }
    }
  },

  async get (action, params) {
    try {
      store.commit('clearErrorMessage')
      const response = await apiClient.get(action, { params: params })
      return response.data
    } catch (error) {
      console.log('GET ERROR -> ', error)
      store.commit('setErrorMessage', error.response ? error.response.data.error : error)
      if (store.state.isAuthenticated && error.response.status === 401) {
        store.commit('clearAuthToken')
        await router.replace({ name: 'login' })
      } else {
        throw error.response ? error.response.data : error
      }
    }
  },
  async post (action, params) {
    try {
      store.commit('clearErrorMessage')
      const response = await apiClient.post(action, params)
      return response.data
    } catch (error) {
      console.log('POST ERROR -> ', error)
      store.commit('setErrorMessage', error.response ? error.response.data.error : error)
      if (store.state.isAuthenticated && error.response.status === 401) {
        store.commit('clearAuthToken')
        await router.replace({ name: 'login' })
      } else {
        throw error.response ? error.response.data : error
      }
    }
  }
}
