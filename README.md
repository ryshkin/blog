# SIMPLE BLOG

## Short description
Server is an expressJS application + MySQL DB
 - root server.js

Client is Vue3 application
 - client

## How to install and run
1. The easiest way to run locally this project is just to run the ``./start-docker`` from the root of project
2. Full local installation:
  - Base requirements:
     Install MySQL >= 5.6
     Install nodeJS >= 14
  - Run installation script from the root of project ``./install``
  - Create the DB by executing initial sql scripts from db/
  - Adjust settings in ``config.js`` for environment ``development``

After installation open the application URL: http://localhost:8080/
And open the email server emulator URL: http://localhost:1080/

For demo yuo can use the following credentials:
login: user1
password: 123

## Structure of the project
```
./client - Vue3 client application
./client/src/config.js - Configuration file which contains all configs for Vue3 client application
./server.js - Main enter point for ExpressJS server
./config.js - Configuration file which contains all configs for ExpressJS server application 
./install - Install script which install all dependencies for ExpressJS server and for Vue3 client applications
./start-docker - Start script which start application in the docker container
```

## How to run unit test
 For now, before testing you need install project as described in option 2.
 Option 1: Run script ``./test`` from the root of project
![](screenshoots/tests1.png)

 Option 2: Run All unit test from your IDE
![](screenshoots/test2.png)
 

## Screenshots
![](screenshoots/screen-register.png)
![](screenshoots/screen-login.png)
![](screenshoots/screen-forgot-password.png)
![](screenshoots/screen-user-profile.png)
![](screenshoots/screen-home.png)
![](screenshoots/screen-search.png)
![](screenshoots/screen-add-post.png)
![](screenshoots/screen-add-comment.png)


## Video example of running Selenium auto-tests:
[![Відео з YouTube](http://img.youtube.com/vi/dVMe1lyLCpE/0.jpg)](http://www.youtube.com/watch?v=dVMe1lyLCpE "Відео з YouTube")


## Video example of running Katalon Studio KDT auto-tests:
[![Відео з YouTube](http://img.youtube.com/vi/66zfQdUQgAY/0.jpg)](http://www.youtube.com/watch?v=66zfQdUQgAY "Відео з YouTube")
