const { Builder, By, Key, until } = require('selenium-webdriver');
const assert = require('assert');
const { takeScreenshot, screenshotInit } = require('./page-objects/screenshotHelper');
const LoginPage = require('./page-objects/loginPage');
const MainPage = require('./page-objects/mainPage');
const PostPage = require('./page-objects/postPage');

describe('POST', function () {
  this.timeout(30000);
  let driver;
  let loginPage;
  let mainPage;
  let postPage;

  beforeEach(async function () {
    driver = await new Builder().forBrowser('chrome').build();
    loginPage = new LoginPage(driver);
    mainPage = new MainPage(driver);
    postPage = new PostPage(driver);
    screenshotInit(__filename);
    await loginPage.open('http://localhost:8080/');
    await driver.manage().window().setRect({ width: 800, height: 900 });
  });

  afterEach(async function () {
    await driver.quit();
  });

  it('POST - Add - OK', async function () {
    const testName = this.test.title;
    await takeScreenshot(driver, testName, 1);
    await loginPage.setUsername('user1');
    await loginPage.setPassword('123');
    await takeScreenshot(driver, testName);
    await loginPage.clickLogin();
    await driver.sleep(500);
    await takeScreenshot(driver, testName);
    await postPage.clickAddPostButton();
    await takeScreenshot(driver, testName);
    await postPage.setTitle('New test post');
    await postPage.setDescription('Description for a new post');
    await postPage.setBody('Some text for the new POST!!!');
    await takeScreenshot(driver, testName);
    await postPage.clickSavePostButton();
    await driver.sleep(500);
    await takeScreenshot(driver, testName);
    const alertText = await driver.findElement(By.css('.alert')).getText();
    assert(alertText.includes('Blog Post posted successfully!'));
    await takeScreenshot(driver, testName);
    await mainPage.clickLogout();
  });

  it('POST - Add comment - OK', async function () {
    const testName = this.test.title;

    await takeScreenshot(driver, testName, 1);

    await loginPage.setUsername('user1');
    await loginPage.setPassword('123');
    await takeScreenshot(driver, testName);

    await loginPage.clickLogin();
    await driver.sleep(500);
    await takeScreenshot(driver, testName);

    await mainPage.clickPostTitle();
    await driver.sleep(500);
    await takeScreenshot(driver, testName);

    const postId = await driver.executeScript("return 'user-' + (new Date().valueOf()) + '@test.net'");
    const comment = `New test comment from Selenium auto test ${postId}`;

    await postPage.setMessage(comment);
    await takeScreenshot(driver, testName);

    await postPage.clickAddCommentButton();
    await driver.sleep(500);
    await takeScreenshot(driver, testName);

    const alertText = await driver.findElement(By.css('.alert')).getText();
    assert(alertText.includes('Comment added to the Post successfully!'));

    const commentText = await driver.findElement(By.css('.list-group:nth-child(3) > .list-group > .list-group-item:nth-child(1) > div')).getText();
    assert(commentText.includes(comment));
    await takeScreenshot(driver, testName);

    await mainPage.clickLogout();
    await takeScreenshot(driver, testName);
  });
});
