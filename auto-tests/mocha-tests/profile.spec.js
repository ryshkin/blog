const { Builder, By, Key, until } = require('selenium-webdriver');
const assert = require('assert');
const LoginPage = require('./page-objects/loginPage');
const ProfilePage = require('./page-objects/profilePage');
const EditProfilePage = require('./page-objects/editProfilePage');
const { takeScreenshot, screenshotInit } = require('./page-objects/screenshotHelper');

describe('Profile', function () {
  this.timeout(30000);
  let driver;
  let loginPage;
  let profilePage;
  let editProfilePage;

  beforeEach(async function () {
    driver = await new Builder().forBrowser('chrome').build();
    loginPage = new LoginPage(driver);
    profilePage = new ProfilePage(driver);
    editProfilePage = new EditProfilePage(driver);
    screenshotInit(__filename);
    await loginPage.open('http://localhost:8080/');
    await driver.manage().window().setRect({ width: 800, height: 900 });

    await loginPage.setUsername('user1');
    await loginPage.setPassword('123');
    await takeScreenshot(driver, this.currentTest.title);
    await loginPage.clickLogin();
    await driver.sleep(500);
    await takeScreenshot(driver, this.currentTest.title);
  });

  afterEach(async function () {
    await driver.quit();
  });

  it('Profile - View', async function () {
    const testName = this.test.title;
    await takeScreenshot(driver, testName, 1);

    await profilePage.clickMyProfile();
    await driver.sleep(500);
    await takeScreenshot(driver, testName);

    const title = await profilePage.getTitle();
    assert.strictEqual(title, 'Your Profile');

    const usernameElement = await profilePage.getUsernameElement();
    assert.strictEqual(await usernameElement.isEnabled(), true);

    const emailElement = await profilePage.getEmailElement();
    assert.strictEqual(await emailElement.isEnabled(), true);

    await takeScreenshot(driver, testName);
    await profilePage.clickLogout();
    await takeScreenshot(driver, testName);
  });

  it('Profile - Edit', async function () {
    const testName = this.test.title;
    await takeScreenshot(driver, testName, 1);

    await profilePage.clickMyProfile();
    await driver.sleep(500);
    await takeScreenshot(driver, testName);

    await editProfilePage.setFirstName('Олег1');
    await editProfilePage.setLastName('Петров1');
    await editProfilePage.setAge('29');
    await editProfilePage.setAddress('New Address');
    await editProfilePage.setWebsite('https://example.com');
    await takeScreenshot(driver, testName);

    await editProfilePage.clickSaveChanges();
    await driver.sleep(500);
    await takeScreenshot(driver, testName);

    const alertText = await editProfilePage.getAlertText();
    assert.strictEqual(alertText, 'Profile updated successfully');

    await takeScreenshot(driver, testName);
    await profilePage.clickLogout();
    await takeScreenshot(driver, testName);
  });
});
