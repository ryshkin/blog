const { By } = require('selenium-webdriver');

class RegisterPage {
    constructor(driver) {
        this.driver = driver;
    }

    async open(url) {
        await this.driver.get(url);
    }

    async setUsername(username) {
        await this.driver.findElement(By.id('username')).sendKeys(username);
    }

    async setEmail(email) {
        await this.driver.findElement(By.id('email')).sendKeys(email);
    }

    async setPassword(password) {
        await this.driver.findElement(By.id('password')).sendKeys(password);
    }

    async setConfirmPassword(confirmPassword) {
        await this.driver.findElement(By.id('confirm_password')).sendKeys(confirmPassword);
    }

    async clickRegister() {
        await this.driver.findElement(By.css('button:nth-child(5)')).click();
    }

    async getAlertText() {
        return await this.driver.findElement(By.css('.alert')).getText();
    }

    async getSuccessAlertText() {
        return await this.driver.findElement(By.css('.alert-success')).getText();
    }

    async clickRegisterLink() {
        await this.driver.findElement(By.linkText('Register')).click();
    }
}

module.exports = RegisterPage;
