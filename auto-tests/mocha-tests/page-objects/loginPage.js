const { By } = require('selenium-webdriver');

class LoginPage {
    constructor(driver) {
        this.driver = driver;
    }

    async open(url) {
        await this.driver.get(url);
    }

    async setUsername(username) {
        await this.driver.findElement(By.id('username')).sendKeys(username);
    }

    async setPassword(password) {
        await this.driver.findElement(By.id('password')).sendKeys(password);
    }

    async clickLogin() {
        await this.driver.findElement(By.xpath("//button[contains(.,'Log In')]")).click();
    }

    async getAlertText() {
        return await this.driver.findElement(By.css('.alert')).getText();
    }
}

module.exports = LoginPage;
