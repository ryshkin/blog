const { By } = require('selenium-webdriver');

class EditProfilePage {
    constructor(driver) {
        this.driver = driver;
    }

    async setFirstName(firstName) {
        const element = await this.driver.findElement(By.id('first_name'));
        await element.clear();
        await element.sendKeys(firstName);
    }

    async setLastName(lastName) {
        const element = await this.driver.findElement(By.id('last_name'));
        await element.clear();
        await element.sendKeys(lastName);
    }

    async setAge(age) {
        const element = await this.driver.findElement(By.id('age'));
        await element.clear();
        await element.sendKeys(age);
    }

    async setAddress(address) {
        const element = await this.driver.findElement(By.id('address'));
        await element.clear();
        await element.sendKeys(address);
    }

    async setWebsite(website) {
        const element = await this.driver.findElement(By.id('website'));
        await element.clear();
        await element.sendKeys(website);
    }

    async clickSaveChanges() {
        await this.driver.findElement(By.css('.btn-primary')).click();
    }

    async getAlertText() {
        return await this.driver.findElement(By.css('.alert')).getText();
    }
}

module.exports = EditProfilePage;
