const { By } = require('selenium-webdriver');

class MainPage {
    constructor(driver) {
        this.driver = driver;
    }

    async open(url) {
        await this.driver.get(url);
    }


    async clickPostTitle() {
        await this.driver.findElement(By.css('.list-group:nth-child(3) > .list-group-item > .mb-1')).click();
    }

    async clickLogout() {
        await this.driver.findElement(By.linkText('Logout')).click();
    }
}

module.exports = MainPage;
