const { By } = require('selenium-webdriver');

class ProfilePage {
    constructor(driver) {
        this.driver = driver;
    }

    async clickMyProfile() {
        await this.driver.findElement(By.linkText('My Profile')).click();
    }

    async getTitle() {
        return await this.driver.findElement(By.css('h1')).getText();
    }

    async getUsernameElement() {
        return await this.driver.findElement(By.id('username'));
    }

    async getEmailElement() {
        return await this.driver.findElement(By.id('email'));
    }

    async clickLogout() {
        await this.driver.findElement(By.linkText('Logout')).click();
    }
}

module.exports = ProfilePage;
