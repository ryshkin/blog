const { By } = require('selenium-webdriver');

class PostPage {
    constructor(driver) {
        this.driver = driver;
    }

    async clickAddPostButton() {
        await this.driver.findElement(By.css('.btn')).click();
    }

    async setTitle(title) {
        await this.driver.findElement(By.id('title')).sendKeys(title);
    }

    async setDescription(description) {
        await this.driver.findElement(By.id('description')).sendKeys(description);
    }

    async setBody(body) {
        await this.driver.findElement(By.id('body')).sendKeys(body);
    }

    async clickSavePostButton() {
        await this.driver.findElement(By.css('.btn-primary')).click();
    }

    async setMessage(message) {
        await this.driver.findElement(By.id('message')).sendKeys(message);
    }

    async clickAddCommentButton() {
        await this.driver.findElement(By.xpath("//button[contains(.,'Add Comment')]")).click();
    }
}

module.exports = PostPage;
