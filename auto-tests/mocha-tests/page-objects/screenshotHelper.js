const fs = require('fs');
const path = require('path');

let lastStep = 0;
let baseName = path.basename(module.parent.filename, '.spec.js');
function getBaseName() {
    return baseName
}


function screenshotInit(fileName) {
    baseName =  path.basename(fileName, '.spec.js');
}

async function takeScreenshot(driver, testName, step) {
    if (step === undefined) {
        step = lastStep + 1;
    }
    lastStep = step;

    testName = testName.replace(/\s+/g, '').replace(/[^a-zA-Z0-9_]/g, '_');
    const testFileName = getBaseName();
    const screenshotName = `${testName}-step${step}.png`;
    const screenshotsFolder = path.resolve(__dirname, '..', 'screenshots', testFileName);
    if (!fs.existsSync(screenshotsFolder)) {
        fs.mkdirSync(screenshotsFolder, { recursive: true });
    }

    const screenshotPath = path.join(screenshotsFolder, screenshotName);
    const data = await driver.takeScreenshot();
    fs.writeFileSync(screenshotPath, data, 'base64');
}

module.exports = {
    takeScreenshot, screenshotInit,
};
