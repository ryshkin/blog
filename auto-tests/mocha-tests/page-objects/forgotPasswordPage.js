const { By } = require('selenium-webdriver');

class ForgotPasswordPage {
    constructor(driver) {
        this.driver = driver;
    }

    async open(url) {
        await this.driver.get(url);
    }

    async clickForgotPasswordLink() {
        await this.driver.findElement(By.linkText("Forgot Password?")).click();
    }

    async setEmail(email) {
        await this.driver.findElement(By.id("email")).sendKeys(email);
    }

    async clickSubmitButton() {
        await this.driver.findElement(By.xpath("//button[contains(.,'Submit')]")).click();
    }

    async getAlertText() {
        return await this.driver.findElement(By.css(".alert")).getText();
    }
}

module.exports = ForgotPasswordPage;
