const { Builder, By, Key, until } = require('selenium-webdriver');
const assert = require('assert');
const RegisterPage = require('./page-objects/registerPage');
const { takeScreenshot, screenshotInit } = require('./page-objects/screenshotHelper');

describe('Registration', function () {
  this.timeout(30000);
  let driver;
  let registerPage;

  beforeEach(async function () {
    driver = await new Builder().forBrowser('chrome').build();
    registerPage = new RegisterPage(driver);
    screenshotInit(__filename);
    await registerPage.open('http://localhost:8080/');
    await driver.manage().window().setRect({ width: 800, height: 900 });
    await registerPage.clickRegisterLink();
  });

  afterEach(async function () {
    await driver.quit();
  });

  it('Registration - False', async function () {
    const testName = this.test.title;
    await takeScreenshot(driver, testName, 1);

    await registerPage.setUsername('user1');
    await registerPage.setEmail('user1@example.com');
    await registerPage.setPassword('123');
    await registerPage.setConfirmPassword('123');
    await takeScreenshot(driver, testName);
    await registerPage.clickRegister();

    await driver.sleep(500);
    await takeScreenshot(driver, testName);
    const alertText = await registerPage.getAlertText();
    assert.strictEqual(alertText, 'AxiosError: Request failed with status code 400');
  });

  it('Registration - OK', async function () {
    const testName = this.test.title;
    await takeScreenshot(driver, testName, 1);

    const uniqueUser = await driver.executeScript("return \'user-\' + (new Date().valueOf()) + \'@test.net\'");
    await registerPage.setUsername(uniqueUser);
    await registerPage.setEmail(uniqueUser);
    await registerPage.setPassword('123');
    await registerPage.setConfirmPassword('123');
    await takeScreenshot(driver, testName);
    await registerPage.clickRegister();

    await driver.sleep(500);
    await takeScreenshot(driver, testName);
    const successAlertText = await registerPage.getSuccessAlertText();
    assert.strictEqual(successAlertText, 'Congrats! Your registration has been successful.');
  });
});
