const { Builder, By, Key, until } = require('selenium-webdriver');
const assert = require('assert');
const { takeScreenshot, screenshotInit } = require('./page-objects/screenshotHelper');
const ForgotPasswordPage = require('./page-objects/forgotPasswordPage');

describe('Forgot Password', function () {
  this.timeout(30000);
  let driver;
  let forgotPasswordPage;

  beforeEach(async function () {
    driver = await new Builder().forBrowser('chrome').build();
    forgotPasswordPage = new ForgotPasswordPage(driver);
    screenshotInit(__filename)
    await forgotPasswordPage.open("http://localhost:8080/");
    await driver.manage().window().setRect({ width: 800, height: 900 });

  });

  afterEach(async function () {
    await driver.quit();
  });

  it('Forgot Password - False', async function () {
    const testName = this.test.title;
    await takeScreenshot(driver, testName, 1);
    await forgotPasswordPage.clickForgotPasswordLink();
    await takeScreenshot(driver, testName);
    await forgotPasswordPage.setEmail("no-user@email.com");
    await takeScreenshot(driver, testName);
    await forgotPasswordPage.clickSubmitButton();
    await driver.sleep(1000);
    await takeScreenshot(driver, testName);
    assert(await forgotPasswordPage.getAlertText() == "No account with that email address exists.");
  });

  it('Forgot Password - OK', async function () {
    const testName = this.test.title;
    await takeScreenshot(driver, testName, 1);
    await driver.findElement(By.id("username")).sendKeys("user1");
    await takeScreenshot(driver, testName);
    await driver.findElement(By.id("password")).sendKeys("123");
    await takeScreenshot(driver, testName);
    await forgotPasswordPage.clickForgotPasswordLink();
    await takeScreenshot(driver, testName);
    await forgotPasswordPage.setEmail("user1@example.com");
    await takeScreenshot(driver, testName);
    await forgotPasswordPage.clickSubmitButton();
    await driver.sleep(1000);
    await takeScreenshot(driver, testName);
    assert(await forgotPasswordPage.getAlertText() == "Successfully sent a restore password link to your email. Please check the email.");
  });
});
