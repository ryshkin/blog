const { Builder, By, Key, until } = require('selenium-webdriver');
const assert = require('assert');
const { takeScreenshot, screenshotInit } = require('./page-objects/screenshotHelper');
const LoginPage = require('./page-objects/loginPage');

describe('Login / Logout functionality', function () {

  this.timeout(30000);
  let driver;
  let loginPage;

  beforeEach(async function () {
    driver = await new Builder().forBrowser('chrome').build();
    loginPage = new LoginPage(driver);
    screenshotInit(__filename)
    await loginPage.open('http://localhost:8080/');
    await driver.manage().window().setRect({ width: 800, height: 900 });
  });

  afterEach(async function () {
    await driver.quit();
  });

  it('Login - OK', async function () {
    const testName = this.test.title;
    await takeScreenshot(driver, testName, 1);
    await loginPage.setUsername('user1');
    await takeScreenshot(driver, testName);
    await loginPage.setPassword('123');
    await takeScreenshot(driver, testName);
    await loginPage.clickLogin();
    await driver.wait(until.elementLocated(By.css('.col-form-label')), 30000);
    await takeScreenshot(driver, testName);
    await driver.findElement(By.linkText('Logout')).click();
  });

  it('Login - False', async function () {
    const testName = this.test.title;
    await takeScreenshot(driver, testName, 1);
    await loginPage.setUsername('user1');
    await takeScreenshot(driver, testName);
    await loginPage.setPassword('1234');
    await takeScreenshot(driver, testName);
    await loginPage.clickLogin();
    await driver.sleep(1000);
    await takeScreenshot(driver, testName);
    assert(await loginPage.getAlertText() === 'Invalid username/password, Try again!');
  });
});
