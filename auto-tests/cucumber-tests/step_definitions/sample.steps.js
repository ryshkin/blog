const { Given, Then, AfterAll, BeforeAll } = require('cucumber');
const { expect } = require('chai');
const puppeteer = require('puppeteer');

let browser;
let page;

BeforeAll(async () => {
    browser = await puppeteer.launch();
});

AfterAll(async () => {
    await browser.close();
});

Given('I visit the homepage', async () => {
    page = await browser.newPage();
    await page.goto('http://localhost:8080/');
});


