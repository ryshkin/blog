const { Builder, By, until } = require('selenium-webdriver');
const { Given, When, Then, AfterAll, BeforeAll, setDefaultTimeout, After, Before } = require('cucumber');
const { expect } = require('chai');

global.prepareInputStr = function (input, symbol) {
   symbol = symbol?symbol:''
   return input.replace(/\s+/g, symbol).toLowerCase()
}



setDefaultTimeout(10*1000) // 10 seconds timeout

global.baseUrl = 'http://localhost:8080/#/'
let logout = async function () {
    await driver.get(baseUrl + 'logout');
}


BeforeAll(async () => {
    global.driver = await new Builder().forBrowser('chrome').build();
});

AfterAll(async () => {
    await global.driver.quit();
});


After(async () => {
    await logout();
})


Given('I am a logged in user', async () => {
    // Add any necessary setup for a logged in user
    await driver.get('http://localhost:8080/#/login');
    await driver.findElement(By.id('username')).sendKeys('user1');
    await driver.findElement(By.id('password')).sendKeys('123');
    await driver.findElement(By.css('button[type="submit"]')).click();
    await driver.wait(until.urlContains('/#/'), 5000);
});

Given('I navigate to the {string} page', async (pageName) => {
    let map = {
        'Registration': 'register',
        'Forgot Password': 'forgot-password'
    }
    pageName = map[pageName]?map[pageName]:pageName;

    if (pageName === 'register') {
        await logout()
    }

    if (['home'].indexOf(pageName.toLowerCase()) > -1) {
        pageName = ''
    }
    await driver.get(baseUrl + pageName.toLowerCase());
});

When('I click on {string} link on the {string} page', async (linkName, pageName) => {
    if (linkName === 'Blog listing') {
        await driver.wait(until.elementLocated(By.css('.list-group:nth-child(3) > .list-group-item > .mb-1')), 1000);
        await driver.findElement(By.css('.list-group:nth-child(3) > .list-group-item > .mb-1')).click();
    } else {
        if (['Add New Post'].indexOf(linkName) > -1) {
            await driver.wait(until.elementLocated(By.xpath("//button[contains(.,'" + linkName + "')]")), 1000);
            await driver.findElement(By.xpath("//button[contains(.,'" + linkName + "')]")).click();
        } else {
            await driver.wait(until.elementLocated(By.linkText(linkName)), 1000);
            await driver.findElement(By.linkText(linkName)).click();
        }
    }

});

When('I click on the {string} button', async (buttonName) => {
   //  await driver.findElement(By.css('button[type="submit"]')).click();
    let map = {
        'Update Profile': 'Save'
    }
    buttonName = map[buttonName]?map[buttonName]:buttonName;
    await driver.findElement(By.xpath("//button[contains(.,'" + buttonName + "')]")).click();
});


When('I fill in {string} with {string}', async (field, value) => {
    field = prepareInputStr(field, '_')

    const randomString = Math.random().toString(36).substring(2, 8);

    if (value === '<random_user>') {
        value = 'user_' + randomString;
    }
    if (value === '<random_email>') {
        value = 'user_' + randomString + '@example.com';
    }

    await driver.findElement(By.id(field)).sendKeys(value);
});


Then('I should land on the {string} page', async (pageName) => {
    // Add assertions for the expected page
});

Then('I should be redirected on the {string} page', async (pageName) => {
    // Add assertions for the expected page
    await driver.wait(until.urlContains('/#/' +  pageName.toLowerCase()), 5000);
});

Then('I should see {string} message as {string}', async (messageType, expectedMessage) => {
    // Add assertions for the expected error message
    await driver.wait(until.elementLocated(By.css('.alert')), 1000);
    const actualMessage = await driver.wait(driver.findElement(By.css('.alert')).getText(), 1000);
    expect(actualMessage).to.equal(expectedMessage);
});

Then('I should see {string} and {string} links', async (link1, link2) => {
    let map = {
        'Dashboard': 'Home'
    }
    link1 = map[link1]?map[link1]:link1;
    link2 = map[link2]?map[link2]:link2;

    await driver.wait(until.elementLocated(By.linkText(link1)), 1000);
    await driver.wait(until.elementLocated(By.linkText(link2)), 1000);

    const link1Element = await driver.findElement(By.linkText(link1));
    const link2Element = await driver.findElement(By.linkText(link2));

    expect(await link1Element.isDisplayed()).to.be.true;
    expect(await link2Element.isDisplayed()).to.be.true;
});

Then('I should see {string} message for {string} field on {string} page', async (formError, inputField, pageName) => {
    // Add assertions for the expected form error message
});

Then('I should see {string} buttton disbaled', async (buttonName) => {
    const buttonElement = await driver.findElement(By.xpath("//button[contains(.,'" + buttonName + "')]"));
    const isDisabled = await buttonElement.getAttribute('disabled');

    expect(isDisabled).to.be.equal('true');
});

Then('I should not be able to submit the {string} form', async (formName) => {
    // Add assertions for the form submission
});

console.log(' --> COMMON !!!')


