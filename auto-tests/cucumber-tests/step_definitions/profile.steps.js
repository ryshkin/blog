const { Builder, By, until } = require('selenium-webdriver');
const { Given, When, Then, AfterAll, BeforeAll, Before, setDefaultTimeout } = require('cucumber');
const { expect } = require('chai');



Then('I should see {string} heading on the Profile page', async (headingText) => {
    // Add assertions for the expected heading text
    const actualHeadingText = await driver.wait(driver.findElement(By.css('h1')).getText(), 1000);
    expect(actualHeadingText).to.equal(headingText);
});



Then('{string} link should be active on the Profile page', async (linkText) => {
    // Add assertions for the expected active link
    await driver.wait(until.elementLocated(By.linkText(linkText)), 1000);
    const isActive = await driver.findElement(By.linkText(linkText)).getAttribute('class');
    expect(isActive).to.include('active');
});


Then('{string} field should be prepopulated and set as "readonly" on the Profile page', async (fieldName) => {
    // Add assertions for the expected prepopulated and readonly field
    fieldName = prepareInputStr(fieldName)
    const fieldValue = await driver.findElement(By.id(fieldName)).getAttribute('value');
    expect(fieldValue).to.not.be.empty;
    expect(await driver.findElement(By.id(fieldName)).getAttribute('readonly')).to.equal('true');
});


Then('{string} field should be prepopulated on the Profile page', async (fieldName) => {
    // Add assertions for the expected prepopulated field
    fieldName = prepareInputStr(fieldName)
    const fieldValue = await driver.findElement(By.id(fieldName)).getAttribute('value');
    expect(fieldValue).to.not.be.empty;
});



When('I fill in First Name as {string}', async (firstName) => {
    // Add code to fill in the first name field
    await driver.wait(until.elementLocated(By.id('first_name')), 1000);

    await driver.findElement(By.id('first_name')).clear();
    await driver.findElement(By.id('first_name')).sendKeys(firstName);
});

When('I fill in Last Name as {string}', async (lastName) => {
    // Add code to fill in the last name field
    await driver.findElement(By.id('last_name')).clear();
    await driver.findElement(By.id('last_name')).sendKeys(lastName);
});

When('I fill in Age as {string}', async (age) => {
    // Add code to fill in the age field
    await driver.findElement(By.id('age')).clear();
    await driver.findElement(By.id('age')).sendKeys(age);
});

When('I fill in Gender as {string}', async (gender) => {
    await driver.findElement(By.id('gender')).sendKeys(gender);
});

When('I fill in Address as {string}', async (address) => {
    await driver.findElement(By.id('address')).clear();
    await driver.findElement(By.id('address')).sendKeys(address);
});

When('I fill in Website as {string}', async (website) => {
    await driver.findElement(By.id('website')).clear();
    await driver.findElement(By.id('website')).sendKeys(website);
});


