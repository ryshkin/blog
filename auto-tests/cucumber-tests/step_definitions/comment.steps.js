const { When, Then } = require('cucumber');
const { By, until } = require('selenium-webdriver');
const { expect } = require('chai');

When('I see a blog listing on the Homepae', async () => {
    // Add any necessary checks to ensure a blog listing is present on the Homepage
});

Then('I should see the comment added to the blog', async () => {
    // Add assertions for the presence of the new comment on the blog
});
