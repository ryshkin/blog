const { Builder, By, until } = require('selenium-webdriver');
const { Given, When, Then, AfterAll, BeforeAll } = require('cucumber');
const { expect } = require('chai');

// Given('I am a logged in user', async () => {
//     // Add any necessary setup for a logged in user
//     await driver.get('http://localhost:8080/#/login');
//     await driver.findElement(By.id('username')).sendKeys('user1');
//     await driver.findElement(By.id('password')).sendKeys('123');
//     await driver.findElement(By.css('button[type="submit"]')).click();
//     await driver.wait(until.urlContains('/#/'), 5000);
// });


// When('I click on {string} link on the {string} page', async (linkName, pageName) => {
//     await driver.wait(until.elementLocated(By.linkText(linkName)), 1000);
//     await driver.findElement(By.linkText(linkName)).click();
// });

Then('I should be successfully logged out', async () => {
    // Add assertions for a successful logout
    await driver.wait(until.urlContains('/#/login'), 5000);
});
