<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Invalid usernamepassword, Try again</name>
   <tag></tag>
   <elementGuidId>9dc8a2f5-739c-4c64-b53d-21ddae08f37e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.alert.alert-dismissible.alert-danger</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>f6c77c9f-f293-442f-b93c-2144fd3ab91e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert alert-dismissible alert-danger</value>
      <webElementGuid>4f3ee502-6000-4538-9e26-d80bfbd00496</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Invalid username/password, Try again!</value>
      <webElementGuid>c63918b1-4739-4969-a9f3-617be942858f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;alert alert-dismissible alert-danger&quot;]</value>
      <webElementGuid>8647296d-a22f-4a54-99a1-544baaac22ce</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div</value>
      <webElementGuid>84851dcc-08a3-428c-ab6c-7a50cd2e9cbc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Register'])[1]/following::div[1]</value>
      <webElementGuid>d964a798-1991-4e83-a1a9-49ef62174ab4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Login'])[1]/following::div[1]</value>
      <webElementGuid>610abc80-f67d-4ebb-8675-4f05ad6c4163</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Login'])[2]/preceding::div[1]</value>
      <webElementGuid>c1f4e0b9-4c6f-4399-b63a-3f43afccd2fc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Username:'])[1]/preceding::div[1]</value>
      <webElementGuid>276a33ef-544c-4c09-afae-6b1b9ec7e1e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Invalid username/password, Try again!']/parent::*</value>
      <webElementGuid>52180528-4119-4b9f-8fe2-efae06790fea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div</value>
      <webElementGuid>9be04ced-9c0e-4d76-9261-99a8699ae0ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = ' Invalid username/password, Try again!' or . = ' Invalid username/password, Try again!')]</value>
      <webElementGuid>2648125e-22cd-495a-8b67-b651cdd3b4fc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
