<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Comment added to the Post successfully</name>
   <tag></tag>
   <elementGuidId>133c41af-0162-406b-9d73-20d75f8cb571</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.alert.alert-dismissible.alert-success</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>983cb58c-dde3-409d-8da0-8c24e6954436</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert alert-dismissible alert-success</value>
      <webElementGuid>d61e251c-982e-4900-8b13-3757760c6c03</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Comment added to the Post successfully!</value>
      <webElementGuid>331573aa-732f-40df-8f1b-3a27c77019a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;alert alert-dismissible alert-success&quot;]</value>
      <webElementGuid>93d3c86a-017e-4c39-bccd-a8a70f78cfd4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div</value>
      <webElementGuid>02ae2faa-9098-47d4-96ff-902e731f360b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Logout'])[1]/following::div[1]</value>
      <webElementGuid>19a26d66-6a3a-4d8c-9621-887e3c8d4225</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My Profile'])[1]/following::div[1]</value>
      <webElementGuid>613c7ad8-b842-4857-97d4-49827d665574</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add New Post'])[1]/preceding::div[2]</value>
      <webElementGuid>bf3d3f13-b410-415d-9b38-fa202c62ba03</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='First name test Last name test'])[1]/preceding::div[3]</value>
      <webElementGuid>fa7727b8-1006-46d1-9504-a802fd618310</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Comment added to the Post successfully!']/parent::*</value>
      <webElementGuid>17702551-ccc6-49c2-a6fd-d0741f9c052c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div</value>
      <webElementGuid>1875b8d1-f0a7-40cc-bb50-9833906103b3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = ' Comment added to the Post successfully!' or . = ' Comment added to the Post successfully!')]</value>
      <webElementGuid>56141765-aa79-466f-8e8e-3832518a2175</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
