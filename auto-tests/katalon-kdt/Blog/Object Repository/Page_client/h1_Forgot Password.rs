<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Forgot Password</name>
   <tag></tag>
   <elementGuidId>f1c8e241-b379-4850-a861-44208da12519</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div[2]/h1</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>15e17d32-8f1e-4a84-907f-7ed392c5c609</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Forgot Password</value>
      <webElementGuid>6ee971b0-39bd-44ad-bcf8-777f03ab56a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;forgot-password user-form&quot;]/h1[1]</value>
      <webElementGuid>52e251e8-5554-4aae-bcec-bc275591b641</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div[2]/h1</value>
      <webElementGuid>a11b08a1-2c24-4aa4-bad4-cfa8f05c363b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='You must be logged in first!'])[1]/following::h1[1]</value>
      <webElementGuid>55d56fc3-8e3b-43b4-87ab-fd8ce99bb260</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Register'])[1]/following::h1[1]</value>
      <webElementGuid>5da49e66-322c-4d2c-830e-cbcf1d70e226</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email:'])[1]/preceding::h1[1]</value>
      <webElementGuid>cbac16e3-a8e1-4745-a7a4-dcc215ab522d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Submit'])[1]/preceding::h1[1]</value>
      <webElementGuid>3be93f02-189e-45c6-8fbf-22b32e1bf97f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Forgot Password']/parent::*</value>
      <webElementGuid>d887cf71-c49c-43c1-bdca-8025198e58c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>2dc77555-df21-4030-be7f-3bdb0c75d180</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Forgot Password' or . = 'Forgot Password')]</value>
      <webElementGuid>f017f7b0-9987-4c6b-a5e6-a113b62a6da1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
