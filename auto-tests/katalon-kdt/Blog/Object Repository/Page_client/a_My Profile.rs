<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_My Profile</name>
   <tag></tag>
   <elementGuidId>3efb764a-5749-4017-a7e1-f2daf9beae39</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/nav/span/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span > a.lead</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>08aa6abb-1b0d-4d00-bb6d-cb185b88161a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#/profile</value>
      <webElementGuid>23742253-b8dc-4afb-8a01-4460fc708087</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>lead</value>
      <webElementGuid>815907e8-6b43-4685-b94f-041a8619ec15</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>My Profile</value>
      <webElementGuid>74db6446-5238-476d-a2db-f6eadf9d1f80</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/nav[1]/span[1]/a[@class=&quot;lead&quot;]</value>
      <webElementGuid>d1d56603-f2c7-49f0-86ad-73c015bf7eed</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/nav/span/a</value>
      <webElementGuid>5090672c-b036-4ca7-87d0-f301de741b26</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'My Profile')]</value>
      <webElementGuid>a67991fb-aaaf-4cec-a9be-e267ecbf31c2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='About'])[1]/following::a[1]</value>
      <webElementGuid>abeeee08-9111-41c3-b4dd-6af2caeb8254</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Logout'])[1]/preceding::a[1]</value>
      <webElementGuid>ba183138-66cd-4664-97a6-3fdb0960a537</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add New Post'])[1]/preceding::a[2]</value>
      <webElementGuid>7bc25399-b8fb-4b46-ac4d-88ded744f934</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='My Profile']/parent::*</value>
      <webElementGuid>d3814ac4-ca2c-4581-af97-6f36e6819f5b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '#/profile')]</value>
      <webElementGuid>20c20d07-4c54-4295-9147-1eaf8bb8c877</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/a</value>
      <webElementGuid>85177e43-38fc-42df-b2ae-6982297f5c02</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#/profile' and (text() = 'My Profile' or . = 'My Profile')]</value>
      <webElementGuid>d804f844-6611-4930-b3b0-745e3a2921ad</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
