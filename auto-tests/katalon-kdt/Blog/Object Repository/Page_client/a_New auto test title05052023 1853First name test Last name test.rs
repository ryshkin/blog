<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_New auto test title05052023 1853First name test Last name test</name>
   <tag></tag>
   <elementGuidId>38737749-6dc4-403c-99e5-2a8e639e056c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[2]/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.list-group-item.list-group-item-action.flex-column.align-items-start.active</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>3a6eee12-ec01-467c-be87-a1766d24f2d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>4f522c66-58cc-46d3-85ec-3e7d1a339408</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>list-group-item list-group-item-action flex-column align-items-start active</value>
      <webElementGuid>8c137124-d77a-40c4-81b4-0ea7ac3360b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>New auto test title05/05/2023 18:53First name test Last name test</value>
      <webElementGuid>c48e6bd5-669f-4bf3-b33f-3f6e4fc88c29</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;layout&quot;]/div[@class=&quot;list-group post&quot;]/a[@class=&quot;list-group-item list-group-item-action flex-column align-items-start active&quot;]</value>
      <webElementGuid>402cc58a-b8c9-42fb-95ad-ba66690a6b5b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[2]/a</value>
      <webElementGuid>12041dc5-e125-428a-92e4-7ddf4f182021</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add New Post'])[1]/following::a[1]</value>
      <webElementGuid>6e8f2e0c-5c7b-4f54-a69a-c3f8199e2454</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Logout'])[1]/following::a[1]</value>
      <webElementGuid>4eeef5ce-7c0d-4292-8b4b-1b6467b373de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='First name test Last name test'])[2]/preceding::a[2]</value>
      <webElementGuid>3f14019b-fa8d-4cbd-85ce-433fe0284a06</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '#')])[5]</value>
      <webElementGuid>6ea7615a-dc13-4a0a-809b-355d932e8847</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/a</value>
      <webElementGuid>5e3ab56d-75a9-40f1-aada-6a3a3bb1c144</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#' and (text() = 'New auto test title05/05/2023 18:53First name test Last name test' or . = 'New auto test title05/05/2023 18:53First name test Last name test')]</value>
      <webElementGuid>35f4cf9b-c893-4152-8b53-74a0bac15f0a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
