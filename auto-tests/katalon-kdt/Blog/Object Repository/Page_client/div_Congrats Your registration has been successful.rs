<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Congrats Your registration has been successful</name>
   <tag></tag>
   <elementGuidId>0e03a907-5358-4978-ba10-a74b11b2f66e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.alert.alert-dismissible.alert-success</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>bf62a2a3-c267-4960-af52-ccc084e4cc4e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert alert-dismissible alert-success</value>
      <webElementGuid>61134af9-3453-45c9-92a7-233466ebb349</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Congrats! Your registration has been successful.</value>
      <webElementGuid>b46ab938-08e3-46c0-90b7-5d1a61aac62b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;alert alert-dismissible alert-success&quot;]</value>
      <webElementGuid>ba7c93f1-9592-4837-845d-743e0ffc1aaf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div</value>
      <webElementGuid>6d2f772e-2d2d-4088-b0b1-7aa19d78dbac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Register'])[1]/following::div[1]</value>
      <webElementGuid>0f7c1bb2-ac1d-43e7-baf6-03735ab89f71</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Login'])[1]/following::div[1]</value>
      <webElementGuid>16c62c86-cc45-4f2f-a64d-a99a6d97bf00</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='You must be logged in first!'])[1]/preceding::div[1]</value>
      <webElementGuid>4ef84ab7-5e43-4a87-89d2-7765af61fa69</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Login'])[2]/preceding::div[2]</value>
      <webElementGuid>19886e59-f207-424e-a3c0-a5f6eb9ca19f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Congrats! Your registration has been successful.']/parent::*</value>
      <webElementGuid>3156cf9e-1401-431f-801f-913c635afd0c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div</value>
      <webElementGuid>bbe64dda-063d-4579-8091-089e76653c41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = ' Congrats! Your registration has been successful.' or . = ' Congrats! Your registration has been successful.')]</value>
      <webElementGuid>3ec1e571-7c01-4725-aa91-453a286eb1bb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
