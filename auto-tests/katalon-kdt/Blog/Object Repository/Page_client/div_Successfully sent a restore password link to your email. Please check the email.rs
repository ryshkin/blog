<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Successfully sent a restore password link to your email. Please check the email</name>
   <tag></tag>
   <elementGuidId>a69bf87d-9770-4948-a1bc-e7092e34dc7b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.alert.alert-dismissible.alert-success</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>c8bfb1cd-6a5b-43d3-a523-e7501d665ed9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert alert-dismissible alert-success</value>
      <webElementGuid>00116487-d33c-4491-9b9e-7369d5dfc230</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Successfully sent a restore password link to your email. Please check the email.</value>
      <webElementGuid>be58b5ad-081b-4122-a3a4-455488aec7a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;alert alert-dismissible alert-success&quot;]</value>
      <webElementGuid>7e9318e6-9ac3-4959-b5ea-5b2aa70ffe86</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div</value>
      <webElementGuid>2843e3b6-28e7-426f-8efa-361cd383b14f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Register'])[1]/following::div[1]</value>
      <webElementGuid>0ace6340-f64a-41c0-89d6-bc4a7baa9681</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Login'])[1]/following::div[1]</value>
      <webElementGuid>03519a21-2215-4a16-8c2d-95b29494e41a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forgot Password'])[1]/preceding::div[1]</value>
      <webElementGuid>3203d84a-9475-4d86-bcba-33c52225c332</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Capture object:'])[1]/preceding::div[2]</value>
      <webElementGuid>34f3f6e0-a6a4-4fb6-8674-9be961ebc03d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Successfully sent a restore password link to your email. Please check the email.']/parent::*</value>
      <webElementGuid>4e0a5258-4748-443f-b5fc-d5f8e16c1370</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div</value>
      <webElementGuid>23b5b4ba-800d-45e1-9952-f201e75c70b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = ' Successfully sent a restore password link to your email. Please check the email.' or . = ' Successfully sent a restore password link to your email. Please check the email.')]</value>
      <webElementGuid>635e8e8b-ca64-4fc2-a812-f1dadc0356c6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
