<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Last Name_last_name</name>
   <tag></tag>
   <elementGuidId>f32249a9-d949-4007-9555-f4bc1b283603</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='last_name']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#last_name</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>a9a738bf-41a8-40e6-8a75-19dd55a6c341</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>a642095a-0846-41d5-8df1-f661e711db06</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>last_name</value>
      <webElementGuid>93db9c80-7168-4da3-9ab1-c92106bde5ab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>479276d3-7d58-487f-b333-e140b69a54f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;last_name&quot;)</value>
      <webElementGuid>b048f8e4-302e-4eab-90f1-adb009d77798</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='last_name']</value>
      <webElementGuid>fca5691c-dbac-4ae5-a0de-5c52179bed9f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/form/div[4]/input</value>
      <webElementGuid>9173bae0-b729-4dcd-8497-621d20383d42</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/input</value>
      <webElementGuid>83642fbf-b7c5-47d0-9d28-9eb1093b58cd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'last_name' and @type = 'text']</value>
      <webElementGuid>b4663309-2d1e-44b8-a882-a1ed0c135daf</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
