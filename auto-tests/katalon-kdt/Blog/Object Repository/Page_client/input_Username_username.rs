<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Username_username</name>
   <tag></tag>
   <elementGuidId>d18959be-7051-472e-bfe5-460fdb85f604</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>IMAGE</key>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#username</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;username&quot;)[count(. | //input[@id = 'username' and @type = 'text']) = count(//input[@id = 'username' and @type = 'text'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>e9c15303-2f5d-4792-89e6-c15e6a9dccbb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>0e58bc80-fc44-45fb-bb57-ebb54783f338</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>username</value>
      <webElementGuid>21b94c5f-76e8-4822-a6d3-0b65fa5a78ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>f6710448-e388-42f4-b8ea-af9627ef1fcd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;username&quot;)</value>
      <webElementGuid>a2c3c377-75df-4f8b-8498-a144348284a5</webElementGuid>
   </webElementProperties>
</WebElementEntity>
