import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('http://localhost:8080/#/login')

WebUI.sendKeys(findTestObject('Page_client/input_Username_username'), 'user1')

WebUI.sendKeys(findTestObject('Page_client/input_Password_password'), '123')

WebUI.click(findTestObject('Page_client/button_Log In'))

WebUI.waitForElementPresent(findTestObject('Page_client/a_My Profile'), 0)

WebUI.click(findTestObject('Page_client/a_My Profile'))

WebUI.waitForElementPresent(findTestObject('Page_client/h1_Your Profile'), 0)

WebUI.setText(findTestObject('Page_client/input_First Name_first_name'), '')

WebUI.sendKeys(findTestObject('Page_client/input_First Name_first_name'), 'First name test')

WebUI.setText(findTestObject('Page_client/input_Last Name_last_name'), '')

WebUI.sendKeys(findTestObject('Page_client/input_Last Name_last_name'), 'Last name test')

WebUI.setText(findTestObject('Page_client/input_Age_age'), '')

WebUI.sendKeys(findTestObject('Page_client/input_Age_age'), '33')

WebUI.sendKeys(findTestObject('Page_client/select_SelectMaleFemaleOther'), 'male')

WebUI.setText(findTestObject('Page_client/input_Address_address'), '')

WebUI.sendKeys(findTestObject('Page_client/input_Address_address'), 'Test adress ...')

WebUI.click(findTestObject('Page_client/button_Save'))

WebUI.waitForElementPresent(findTestObject('Page_client/div_Profile updated successfully'), 0)

WebUI.click(findTestObject('Page_client/a_Logout'))

WebUI.closeBrowser()

