import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.util.Random as Random

String randomUsername = generateRandomUsername()

WebUI.openBrowser('')

WebUI.navigateToUrl('http://localhost:8080/#/register')

WebUI.sendKeys(findTestObject('Page_client/input_Username_username'), randomUsername)

WebUI.sendKeys(findTestObject('Page_client/input_Email_email'), randomUsername + '@test.dev')

WebUI.sendKeys(findTestObject('Page_client/input_Password_password'), '123')

WebUI.sendKeys(findTestObject('Page_client/input_Confirm Password_confirm_password'), '123')

WebUI.click(findTestObject('Page_client/button_Register Now'))

WebUI.waitForElementPresent(findTestObject('Page_client/div_Congrats Your registration has been successful'), 0)

WebUI.navigateToUrl('http://localhost:8080/#/login')

WebUI.sendKeys(findTestObject('Page_client/input_Username_username'), randomUsername)

WebUI.sendKeys(findTestObject('Page_client/input_Password_password'), '123')

WebUI.click(findTestObject('Page_client/button_Log In'))

WebUI.waitForElementPresent(findTestObject('Page_client/a_Logout'), 0)

WebUI.click(findTestObject('Page_client/a_Logout'))

WebUI.closeBrowser()

def generateRandomUsername() {
    String characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

    int length = 10

    Random random = new Random()

    StringBuilder sb = new StringBuilder(length)

    for (int i = 0; i < length; i++) {
        sb.append(characters.charAt(random.nextInt(characters.length())))
    }
    
    return 'user_' + sb.toString()
}

