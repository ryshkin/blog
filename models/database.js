const mysql = require('mysql2');

function createDbPoolConnections(config) {
    return mysql.createPool({
        ...config, ...{
            waitForConnections: true,
            connectionLimit: 10,
            queueLimit: 0
        }
    }).promise();
}

module.exports = createDbPoolConnections;
