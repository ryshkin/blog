const bcrypt = require('bcrypt');

class User {
    constructor(database) {
        this.db = database;
    }

    async addUser(username, email, password) {
         const hashedPassword = await bcrypt.hash(password.toString(), 10);
         const [r] = await this.db.query(
             "INSERT INTO user (username, email, password) VALUES (?, ?, ?)",
             [username, email, hashedPassword]
         );
         return r.insertId;
    }
    async checkPassword(hpassword, password) {
        return await bcrypt.compare(password.toString(), hpassword);
    }
    async getUser(username) {
        const [r, f] = await this.db.query(
            "SELECT * FROM user WHERE username = ?",
            [username]
        );
        return r[0];
    }
    async getUserByEmail(email) {
        const [r, f] = await this.db.query(
            "SELECT * FROM user WHERE email = ?",
            [email]
        );
        return r[0];
    }
    async getUserProfile(id) {
        const [r, f] = await this.db.query(
            "SELECT * FROM profile WHERE user_id = ?",
            [id]
        );
        return r[0];
    }
    async updateUserProfile(profile) {
        const [r] = await this.db.query(
            `
                INSERT INTO profile (user_id, first_name, last_name, age, gender, address, website)
                VALUES (?, ?, ?, ?, ?, ?, ?)
                ON DUPLICATE KEY UPDATE first_name = VALUES(first_name),
                                        last_name  = VALUES(last_name),
                                        age        = VALUES(age),
                                        gender     = VALUES(gender),
                                        address    = VALUES(address),
                                        website    = VALUES(website);
            `,
            [
                profile.user_id,
                profile.first_name,
                profile.last_name,
                profile.age,
                profile.gender,
                profile.address,
                profile.website
            ]
        );
        return r.affectedRows > 0;
    }

    async updateUserPassword(username, password) {
       const hashedPassword = await bcrypt.hash(password.toString(), 10);
       const [r] = await this.db.query(
            "UPDATE user SET password = ? WHERE username = ?",
            [hashedPassword, username]
       );
       return r.affectedRows > 0;
    }
    async deleteUser(username) {
        const [r] = await this.db.query(
            "DELETE FROM user WHERE username = ?",
            [username]
        );
        return r.affectedRows > 0;
    }

}

module.exports = User;
