class Post {
    constructor(database) {
        this.db = database;
    }

    async add(user_id, title, description, body) {
        const [r] = await this.db.query(
            "INSERT INTO post (user_id, title, description, body) VALUES (?, ?, ?, ?)",
            [user_id, title, description, body]
        );
        return r.insertId;
    }

    async update(user_id, post_id, title, description, body) {
        const [r] = await this.db.query(
            `
                UPDATE post
                SET title       = ?,
                    description = ?,
                    body        = ?
                WHERE id = ? and user_id =?;
            `,
            [title, description, body, post_id, user_id]
        );
        return r.changedRows;
    }

    async search(page = 1, limit = 10, searchTerm = '') {
        const offset = (page - 1) * limit;
        const searchCondition = searchTerm
            ? `WHERE title LIKE ? OR description LIKE ? OR profile.first_name LIKE ? OR profile.last_name LIKE ?`
            : '';
        const query = `
            SELECT post.*,  concat_ws(' ', profile.first_name, profile.last_name) as author
            FROM post
            LEFT JOIN profile on profile.user_id = post.user_id    
            ${searchCondition}
            ORDER BY created_at DESC
            LIMIT ? OFFSET ?;
        `;
        try {
            const [rows] = searchTerm ? await this.db.query(query, [
                    `%${searchTerm}%`,
                    `%${searchTerm}%`,
                    `%${searchTerm}%`,
                    `%${searchTerm}%`,
                    limit,
                    offset,
                ]) : await this.db.query(query, [limit, offset]);
            return rows;
        } catch (error) {
            throw new Error('Error fetching posts: ' + error.message);
        }
    }

    async addComment(user_id, post_id, name, message) {
        const [r] = await this.db.query(
            "INSERT INTO comments (user_id, post_id, name, message) VALUES (?, ?, ?, ?)",
            [user_id, post_id, name, message]
        );
        return r.insertId;
    }

    async get(post_id) {
        const query = `
            SELECT post.*,  concat_ws(' ', profile.first_name, profile.last_name) as author
            FROM post
            LEFT JOIN profile on profile.user_id = post.user_id    
            WHERE post.id = ?
        `;
        try {
            const [rows] = await this.db.query(query, [post_id]);
            return rows.length > 0?rows[0]:null;
        } catch (error) {
            throw new Error('Error fetching post: ' + error.message);
        }
    }

    async getPostComments(post_id, limit = null) {
        let query = `
            SELECT *
            FROM comments
            WHERE post_id = ?
            order by created_at desc
        `;
        if (limit) {
            query += ' limit 0, ' + limit;
        }
        try {
            const [rows] =  await this.db.query(query, [post_id]);
            return rows;
        } catch (error) {
            throw new Error('Error fetching post: ' + error.message);
        }
    }
}

module.exports = Post;
