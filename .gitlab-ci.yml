image: node:lts-alpine

stages:
  - build
  - lint
  - test
  - code_quality_analysis
  - mocha_tests
  - selenium_test
  - cucumber_tests
  - katalon_test
  - insider_scan

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - node_modules/
    - client/node_modules/

build:
  stage: build
  script:
    - echo "Install all dependencies for server and client"
    - npm install
    - cd client && npm install --force
  artifacts:
    paths:
      - node_modules/
      - client/node_modules/
    expire_in: 1 week
  rules:
    - changes:
        - package.json
        - package-lock.json
        - client/package.json
        - client/package-lock.json

lint:
  stage: lint
  script:
    - echo "Run lint checker for code quality"
    #- npm run lint
    - cd client && npm run lint
  dependencies:
    - build

test-unit-tests:
  stage: test
  services:
    - mariadb:10.5
  variables:
    MYSQL_DATABASE: blog
    MYSQL_ROOT_PASSWORD: root
  before_script:
    - apk add --no-cache mariadb-client
    - >
      echo "Waiting for MySQL service to be ready..." &&
      for i in `seq 1 60`;
      do
        mysqladmin ping -h mariadb -u root -proot --silent && echo "MySQL service is ready" && break ||
        sleep 2;
      done &&
      echo "MySQL service reached the maximum retry count" || true
  script:
    - echo "SELECT 'OK';" | mysql --user=root --password="$MYSQL_ROOT_PASSWORD" --host=mariadb "$MYSQL_DATABASE"
    - mysql -u root -proot -h mariadb < db/create.sql
    - echo "Run unit test"
    - npm run test
    #- cd client && npm run test
  coverage: '/All files[^|]*\|[^|]*\s+(\d+\.\d+)/'
  artifacts:
    when: always
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage/cobertura-coverage.xml
      junit:
        - junit.xml
  dependencies:
    - build

selenium_test:
  stage: selenium_test
  image: node:lts-alpine
  before_script:
    - apk update && apk add openjdk11-jre
    - apk add chromium
    - npm install -g selenium-side-runner
    - wget https://chromedriver.storage.googleapis.com/94.0.4606.61/chromedriver_linux64.zip
    - unzip chromedriver_linux64.zip
    - mv chromedriver /usr/local/bin/
    - chmod +x /usr/local/bin/chromedriver
  script:
    - selenium-side-runner -c "webdriver.chrome.driver=/usr/local/bin/chromedriver;browserName=chrome" auto-tests/Blog.side
  dependencies:
    - build

sonarqube-check:
  stage: code_quality_analysis
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
    GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - sonar-scanner -X
  allow_failure: true
  rules:
    - if: $CI_COMMIT_BRANCH == 'main'

katalon_test:
  stage: katalon_test
  image: katalonstudio/katalon
  variables:
    KATALON_API_KEY: 'your_katalon_api_key'
  script:
    - katalon-execute.sh -browserType="Chrome" -retry=0 -statusDelay=15 -testSuitePath="auto-tests/Blog/Test Suites/AllTestSuites" -apiKey="$KATALON_API_KEY" --info -noSplash
  artifacts:
    when: always
    paths:
      - report/
  dependencies:
    - build

mocha_tests:
  stage: test
  image: node:lts-alpine
  before_script:
    - apk update && apk add openjdk11-jre
    - apk add chromium
    - cd auto-tests/mocha && npm ci
    - wget https://chromedriver.storage.googleapis.com/94.0.4606.61/chromedriver_linux64.zip
    - unzip chromedriver_linux64.zip
    - mv chromedriver /usr/local/bin/
    - chmod +x /usr/local/bin/chromedriver
  script:
    - cd auto-tests/mocha && npm test
  artifacts:
    when: always
    paths:
      - test-reports/mocha/
      - auto-tests/mocha/screenshots/
    expire_in: 1 week
  dependencies:
    - build

cucumber_tests:
  stage: cucumber_tests
  image: node:lts-alpine
  before_script:
    - cd auto-tests/cucumber-tests && npm ci
    - apk update && apk add openjdk11-jre
    - apk add chromium
    - wget https://chromedriver.storage.googleapis.com/94.0.4606.61/chromedriver_linux64.zip
    - unzip chromedriver_linux64.zip
    - mv chromedriver /usr/local/bin/
    - chmod +x /usr/local/bin/chromedriver
  script:
    - ./run-cucumber-tests
  artifacts:
    when: always
    paths:
      - test-reports/cucumber/
    expire_in: 1 week
  dependencies:
    - build

insider_scan:
  stage: code_quality_analysis
  image: insidersec/insider:latest
  script:
    - insider -tech javascript -target .
  artifacts:
    paths:
      - ./insider-report.html
      - ./insider-report.json
    expire_in: 1 week
  dependencies:
    - build
