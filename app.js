const env = process.env.NODE_ENV || 'development';
const cors = require('cors');
let config = require('./config')
config = config[env]

const createDbPoolConnections = require('./models/database');
const User = require('./models/user');
const Post = require('./models/post');
const express = require('express');
const bodyParser = require('body-parser');
const nodemailer = require('nodemailer');



const app = express();
app.use(cors());
app.set('env', env);
app.set('config', config);

// parse application/json
app.use(bodyParser.json())

const db = createDbPoolConnections(config.db);
app.set('db', db);

const mailer = nodemailer.createTransport(config.mail);
app.set('mailer', mailer);

const user = new User(db);
app.set('user', user);

const post = new Post(db);
app.set('post', post);


const baseApi = require('./api/base');
const userApi = require('./api/user');
const postApi = require('./api/post');

// set up routes
app.use('/api', baseApi);
app.use('/api/user', userApi);
app.use('/api/post', postApi);
app.get('/', (req, res) => {
    res.send('Welcome to the BLOG app!');
});


module.exports = app;
