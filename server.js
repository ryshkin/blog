const app = require("./app");

// start server
const config = app.get('config');
app.listen(config.serverPort, async () => {
    console.log(`Server is running on port ${config.serverPort}`);
    console.log(`Environment: ${app.get('env')}`);
    console.log(`Config: ${JSON.stringify(config, null, 4)}`);
});
