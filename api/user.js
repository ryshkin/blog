const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const {authenticate, logout} = require('./auth');


// POST registration endpoint
router.post('/register', async (req, res) => {
    const { username, email, password, confirm_password } = req.body;

    // Check if any required field is empty
    if (!username) {
        return res.status(400).json({ error: 'The username is required and cannot be empty', input_field: 'username' });
    }
    if (!email) {
        return res.status(400).json({ error: 'The email is required and cannot be empty', input_field: 'email' });
    }
    if (!password) {
        return res.status(400).json({ error: 'The password is required and cannot be empty', input_field: 'password' });
    }
    if (!confirm_password) {
        return res.status(400).json({ error: 'The confirm password is required and cannot be empty', input_field: 'confirm_password' });
    }

    // Check if email is valid
    const emailRegex = /^\S+@\S+\.\S+$/;
    if (!emailRegex.test(email)) {
        return res.status(400).json({ error: 'The email address is not valid', input_field: 'email' });
    }

    // Check if password and confirm_password match
    if (password !== confirm_password) {
        return res.status(400).json({ error: 'The password and its confirm are not the same', input_field: 'confirm_password' });
    }

    // Save user to database or other data source
    const user = req.app.get('user');
    // Save the new user to database or other data source here
    try {
        await user.addUser(username, email, password);
        return res.status(201).json({ message: 'Congrats! Your registration has been successful.' });
    } catch (e) {
        return res.status(400).json({
            status: 'DB error',
            error: e.toString()
        });
    }
});

// POST login endpoint
router.post('/login', async (req, res) => {
    const config = req.app.get('config');
    const user = req.app.get('user');
    const { username, password } = req.body;
    if (!username || !password) {
        res.status(400).json({ error: 'Username and password are required' });
        return;
    }
    try {
        const u = await user.getUser(username);
        if (u && await user.checkPassword(u.password, password)) {
            const token = jwt.sign({ username }, config.secretKey, { expiresIn: config.authTTL });
            res.status(200).json({ message: 'Login successful', token });
        } else {
            res.status(401).json({ error: 'Invalid username/password, Try again!' });
        }
    } catch (e) {
        res.status(500).json({ error: e.toString() });
    }

});

// GET logout endpoint
router.get('/logout', authenticate, (req, res) => {
    const authHeader = req.headers.authorization;
    const token = authHeader.split(' ')[1];
    logout(token);
    res.status(200).json({ message: 'Logout successful' });
});

// POST forgot-password endpoint
router.post('/forgot-password', async (req, res) => {
    const config = req.app.get('config');
    const user = req.app.get('user');
    const mailer = req.app.get('mailer');
    const { email } = req.body;
    if (!email) {
        res.status(400).json({ error: 'Email is required' });
        return;
    }
    const u = await user.getUserByEmail(email);
    if (u) {
       const token = jwt.sign({ username: u.username }, config.secretKey, { expiresIn: config.authTTL });
       const baseUrl = config.appBaseUrl;
       const resetLink = `${baseUrl}#/reset-password/${token}`;
       try {
           await mailer.sendMail({
               from: config.mailFrom,
               to: u.email,
               subject: 'Blog - Password Reset',
               html: `
                    <body style="color: black; background-color: white;">
                        <p>Please use the following link to reset your password: <a href="${resetLink}">${resetLink}</a></p>
                    </body>
                    `
           });
           res.status(200).json({ message: 'Password reset email sent' });
       } catch (e) {
           res.status(500).json({ error: 'Email error' });
           return;
       }
    } else {
      res.status(404).json({ error: 'No account with that email address exists.' });
      return;
    }
});

// POST reset-password endpoint
router.post('/reset-password', async (req, res) => {
    const config = req.app.get('config');
    const user = req.app.get('user');
    const { token, newPassword } = req.body;
    if (!token || !newPassword) {
        res.status(400).json({ error: 'Token and new password are required' });
        return;
    }
    try {
        const decoded = jwt.verify(token, config.secretKey);
        const result = await user.updateUserPassword(decoded.username, newPassword);
        if (result === true) {
          res.status(200).json({ message: 'Password reset successful' });
        } else {
          res.status(404).json({ error: 'User not found' });
          return;
        }
    } catch (err) {
        res.status(401).json({ error: 'Password reset token is invalid or has expired.' });
    }
});

// GET user profile endpoint
router.get('/profile', authenticate, async (req, res) => {
    const user = req.app.get('user');
    const currentUser = req.user;
    const u = await user.getUser(currentUser.username);
    if (u) {
      let profile = Object.assign({
          username: u.username,
          email: u.email,
          first_name: null,
          last_name: null,
          age: null,
          gender: null,
          address: null,
          website: null,
      }, await user.getUserProfile(u.id));
      res.status(200).json(profile);
    } else {
      res.status(404);
      throw new Error('User not found');
    }
});

// POST user profile endpoint
router.post('/profile', authenticate, async (req, res) => {
    const user = req.app.get('user');
    const currentUser = req.user;
    const u = await user.getUser(currentUser.username);
    if (u) {
        let profile = Object.assign({
            user_id: u.id,
            first_name: null,
            last_name: null,
            age: null,
            gender: null,
            address: null,
            website: null,
        }, req.body);
        await user.updateUserProfile(profile);
        res.status(200).json(profile);
    } else {
        res.status(404);
        throw new Error('User not found');
    }
});

router.delete('/user', async (req, res) => {
    const { username, token } = req.body;
    const user = req.app.get('user');
    const config = req.app.get('config');
    if (token === config.adminToken) {
        return res.status(201).json(
            {
                ok: await user.deleteUser(username),
                username: username,
            });
    } else {
        return res.status(201).json(
            {
                ok: false,
                msg: 'you haven`t rights to do this',
                username: username,
            });
    }


});

module.exports = router;
