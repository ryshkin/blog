const express = require('express');
const router = express.Router();

router.get('/ping', async (req, res) => {
    let result = {
       server: 'OK',
       database: 'NO',
    }
    try {
        const db = req.app.get('db');
        const [rows, fields] = await db.query("select now() as date");
        result.database = 'OK';
        result.time = rows[0].date;
    } catch (e) {
        result.error = e.toString();
    }
    res.json(result);
});

module.exports = router;
