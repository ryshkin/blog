const jwt = require('jsonwebtoken');
const tokenBlacklist = new Set();

const authenticate = (req, res, next) => {
    const config = req.app.get('config');
    const authHeader = req.headers.authorization;
    if (!authHeader) {
        res.status(401).json({ error: 'Authorization header is required' });
        return;
    }
    const token = authHeader.split(' ')[1];
    if (tokenBlacklist.has(token)) {
        res.status(401).json({ error: 'Token is blacklisted' });
        return;
    }
    try {
        const decoded = jwt.verify(token, config.secretKey);
        req.user = decoded;
        next();
    } catch (err) {
        if (err.name === 'TokenExpiredError') {
            res.status(401).json({ error: 'Token expired' });
        } else {
            res.status(401).json({ error: 'Invalid token' });
        }
    }
};
const logout = (token) => {
   tokenBlacklist.add(token);
}

module.exports =  {
    authenticate,
    logout,
    tokenBlacklist
};

