const express = require('express');
const router = express.Router();
const {authenticate} = require('./auth');

// POST add blog post endpoint
router.post('/add', authenticate, async (req, res) => {
    const { title, description, body } = req.body;
    // Check if any required field is empty
    if (!title) {
        return res.status(400).json({ error: 'The title is required and cannot be empty', input_field: 'title' });
    }
    if (!description) {
        return res.status(400).json({ error: 'The description is required and cannot be empty', input_field: 'description' });
    }
    if (!body) {
        return res.status(400).json({ error: 'The body is required and cannot be empty', input_field: 'body' });
    }

    const user = req.app.get('user');
    const post = req.app.get('post');
    const currentUser = req.user;
    const u = await user.getUser(currentUser.username);
    if (u) {
        try {
            // Save the blog post to the database
            const postId = await post.add(u.id, title, description, body);
            res.status(201).json({ message: 'Blog Post posted successfully!', postId });
        } catch (e) {
            res.status(400).json({ error: e.toString() });
        }
    } else {
        res.status(404);
        throw new Error('User not found');
    }
});

// PUT update blog post endpoint
router.put('/update/:postId', authenticate, async (req, res) => {
    const { postId } = req.params;
    const { title, description, body } = req.body;

    // Check if any required field is empty
    if (!title) {
        return res.status(400).json({ error: 'The title is required and cannot be empty', input_field: 'title' });
    }
    if (!description) {
        return res.status(400).json({ error: 'The description is required and cannot be empty', input_field: 'description' });
    }
    if (!body) {
        return res.status(400).json({ error: 'The body is required and cannot be empty', input_field: 'body' });
    }

    const user = req.app.get('user');
    const post = req.app.get('post');
    const currentUser = req.user;
    const u = await user.getUser(currentUser.username);
    if (u) {
        try {
            // Update the blog post in the database
            const changedRows = await post.update(u.id, postId, title, description, body);
            if (changedRows) {
                res.status(200).json({ message: 'Blog Post updated successfully!', postId: postId });
            } else {
                res.status(404).json({ error: 'Post not found or you do not have permission to edit it' });
            }
        } catch (e) {
            res.status(400).json({ error: e.toString() });
        }
    } else {
        res.status(404);
        throw new Error('User not found');
    }
});

// Get blog post endpoint
router.get('/post/:postId', authenticate, async (req, res) => {
    const { postId } = req.params;
    const post = req.app.get('post');
    try {
        const blogPost = await post.get(postId);
        if (blogPost) {
            res.status(200).json(blogPost);
        } else {
            res.status(404).json({error: 'Post not found or you do not have permission to get it'});
        }
    } catch (e) {
        res.status(400).json({error: e.toString()});
    }
});

// Get blog post comments endpoint
router.get('/comments/:postId', authenticate, async (req, res) => {
    const { postId } = req.params;
    const post = req.app.get('post');
    try {
        const blogComments = await post.getPostComments(postId);
        res.status(200).json(blogComments);
    } catch (e) {
        res.status(400).json({error: e.toString()});
    }
});

// GET posts endpoint with pagination and search
router.get('/search', async (req, res) => {
    const post = req.app.get('post');
    const page = parseInt(req.query.page) || 1;
    const limit = parseInt(req.query.limit) || 10;
    const searchTerm = req.query.search || '';
    try {
        const posts = await post.search(page, limit, searchTerm);
        for (let p of posts) {
            p['comments'] = await post.getPostComments(p.id, 3)
        }
        res.status(200).json(posts);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
});

// POST add comment endpoint
router.post('/add/comment', authenticate, async (req, res) => {
    const { postId, name, message } = req.body;
    // Check if any required field is empty
    if (!name) {
        return res.status(400).json({ error: 'The name is required and cannot be empty', input_field: 'name' });
    }
    if (!message) {
        return res.status(400).json({ error: 'The message is required and cannot be empty', input_field: 'message' });
    }

    const user = req.app.get('user');
    const post = req.app.get('post');
    const currentUser = req.user;
    const u = await user.getUser(currentUser.username);
    if (u) {
        try {
            // Save the comment to the database
            const commentId = await post.addComment(u.id, postId, name, message);
            res.status(201).json({ message: 'Comment added to the Post successfully!', commentId });
        } catch (e) {
            res.status(400).json({ error: e.toString() });
        }
    } else {
        res.status(404).json({ error: 'User not found', input_field: currentUser.username });
    }
});

module.exports = router;
