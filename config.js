const config = {
    development: {
        appBaseUrl: 'http://localhost:8080/',
        serverPort: 3000,
        secretKey: 'secret key',
        authTTL: '1h',
        db: {
            host: 'localhost',
            user: 'root',
            password: 'root',
            database: 'blog'
        },
        mailFrom: 'support@blog.dev',
        mail: {
            host: 'localhost',
            port: 1025,
            ignoreTLS: true
        },
        adminToken: 'f865b53623b121fd34ee5426c792e5c33af8c227'
    },
    test: {
        appBaseUrl: 'http://localhost:8080/',
        serverPort: 3000,
        secretKey: 'secret key',
        authTTL: '1h',
        db: {
            host: 'mariadb',
            user: 'root',
            password: 'root',
            database: 'blog'
        },
        mailFrom: 'support@blog.dev',
        mail: {
            host: 'localhost',
            port: 1025,
            ignoreTLS: true
        },
        adminToken: 'f865b53623b121fd34ee5426c792e5c33af8c227'
    },
    test_local: {
        appBaseUrl: 'http://localhost:8080/',
        serverPort: 3000,
        secretKey: 'secret key',
        authTTL: '1h',
        db: {
            host: 'localhost',
            user: 'root',
            password: 'root',
            database: 'blog'
        },
        mailFrom: 'support@blog.dev',
        mail: {
            host: 'localhost',
            port: 1025,
            ignoreTLS: true
        },
        adminToken: 'f865b53623b121fd34ee5426c792e5c33af8c227'
    },
    production: {
        appBaseUrl: 'http://localhost:8080/',
        serverPort: 3000,
        secretKey: 'secret key',
        authTTL: '1h',
        db: {
            host: 'db',
            user: 'root',
            password: 'root',
            database: 'blog'
        },
        mailFrom: 'support@blog.dev',
        mail: {
            host: 'localhost',
            port: 1025,
            ignoreTLS: true
        },
        adminToken: 'f865b53623b121fd34ee5426c792e5c33af8c227'
    }
}

module.exports = config
