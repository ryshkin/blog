DROP DATABASE IF EXISTS `blog`;
CREATE DATABASE IF NOT EXISTS `blog` CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE blog;
CREATE TABLE `user`
(
    `id`       int          NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `username` varchar(100) NOT NULL,
    `email`    varchar(100) NOT NULL,
    `password` varchar(100) NOT NULL
) ENGINE = 'InnoDB';
ALTER TABLE `user`
    ADD UNIQUE `username_email` (`username`, `email`);

CREATE TABLE profile
(
    id         int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    user_id    int NOT NULL,
    first_name varchar(100),
    last_name  varchar(100),
    age        int,
    gender     varchar(10),
    address    varchar(255),
    website    varchar(255),
    FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE
) ENGINE = 'InnoDB';
ALTER TABLE `profile`
    ADD UNIQUE `user_id` (`user_id`),
    DROP INDEX `user_id`;

CREATE TABLE `post`
(
    `id`          INT AUTO_INCREMENT PRIMARY KEY,
    `user_id`     INT NOT NULL,
    `title`       VARCHAR(255) NOT NULL,
    `description` VARCHAR(512) NOT NULL,
    `body`        TEXT         NOT NULL,
    `created_at`  DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at`  DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)  ON DELETE CASCADE
) ENGINE = 'InnoDB';

CREATE TABLE `comments`
(
    `id`         INT AUTO_INCREMENT PRIMARY KEY,
    `user_id`    INT          NOT NULL,
    `post_id`    INT          NOT NULL,
    `name`       VARCHAR(255) NOT NULL,
    `message`    TEXT         NOT NULL,
    `created_at` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)  ON DELETE CASCADE,
    FOREIGN KEY (`post_id`) REFERENCES `post` (`id`)  ON DELETE CASCADE
) ENGINE = 'InnoDB';




