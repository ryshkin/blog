const request = require('supertest');
const app = require('../app');
const jwt = require("jsonwebtoken");

const config = app.get('config');
const db = app.get('db');
const user = app.get('user');


describe('POST /api/user/reset-password', () => {
    beforeAll(async () => {
        await user.deleteUser('existed-user');
        await user.addUser('existed-user', 'existed-user@example.com', 'valid');
    })

    it('should return 400 if token or newPassword is missing', async () => {
        const res = await request(app).post('/api/user/reset-password').send({});
        expect(res.status).toEqual(400);
        expect(res.body).toHaveProperty('error', 'Token and new password are required');
    });

    it('should return 401 if token is invalid', async () => {
        const res = await request(app).post('/api/user/reset-password').send({ token: 'invalid_token', newPassword: 'new_password' });
        expect(res.status).toEqual(401);
        expect(res.body).toHaveProperty('error', 'Password reset token is invalid or has expired.');
    });

    it('should return 404 if user is not found', async () => {
        const token = jwt.sign({ username: 'not-existing-user' }, config.secretKey, { expiresIn: config.authTTL });
        const res = await request(app).post('/api/user/reset-password').send({ token, newPassword: 'new_password' });
        expect(res.status).toEqual(404);
        expect(res.body).toHaveProperty('error', 'User not found');
    });

    it('should return 200 and reset the password if token is valid and user is found', async () => {
        const token = jwt.sign({ username: 'existed-user' }, config.secretKey, { expiresIn: config.authTTL });
        const res = await request(app).post('/api/user/reset-password').send({ token, newPassword: 'new_password' });
        expect(res.status).toEqual(200);
        expect(res.body).toHaveProperty('message', 'Password reset successful');
    });

    afterAll(async () => {
        await user.deleteUser('existed-user');
        await db.end();
    })
});
