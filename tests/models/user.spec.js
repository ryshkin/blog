const User = require('../../models/user');
const bcrypt = require('bcrypt');

jest.mock('bcrypt', () => ({
    hash: jest.fn(),
    compare: jest.fn(),
}));

describe('User Model', () => {
    const mockDb = {
        query: jest.fn(),
    };
    const user = new User(mockDb);

    afterEach(() => {
        jest.clearAllMocks();
    });

    // Arrange
    const username = 'testUser';
    const email = 'test@example.com';
    const password = 'password123';
    const hashedPassword = 'hashedPassword123';
    const hpassword = 'hashedPassword123';
    const insertId = 1;

    const profile = {
        user_id: 1,
        first_name: 'John',
        last_name: 'Doe',
        age: 30,
        gender: 'M',
        address: '123 Street',
        website: 'https://johndoe.com'
    };

    // addUser tests
    describe('addUser', () => {
        it('should call hash and query with the correct parameters and return the insertId', async () => {
            bcrypt.hash.mockResolvedValue(hashedPassword);
            mockDb.query.mockResolvedValue([{ insertId }]);

            // Act
            const result = await user.addUser(username, email, password);

            // Assert
            expect(bcrypt.hash).toHaveBeenCalledWith(password, 10);
            expect(mockDb.query).toHaveBeenCalledWith(
                'INSERT INTO user (username, email, password) VALUES (?, ?, ?)',
                [username, email, hashedPassword]
            );
            expect(result).toBe(insertId);
        });
    });

    // checkPassword tests
    describe('checkPassword', () => {
        it('should call compare with the correct parameters and return true if passwords match', async () => {
            bcrypt.compare.mockResolvedValue(true);

            // Act
            const result = await user.checkPassword(hpassword, password);

            // Assert
            expect(bcrypt.compare).toHaveBeenCalledWith(password, hpassword);
            expect(result).toBe(true);
        });

        it('should call compare with the correct parameters and return false if passwords do not match', async () => {
            bcrypt.compare.mockResolvedValue(false);

            // Act
            const result = await user.checkPassword(hpassword, password);

            // Assert
            expect(bcrypt.compare).toHaveBeenCalledWith(password, hpassword);
            expect(result).toBe(false);
        });

        it('should throw an error if compare fails', async () => {
            // Arrange
            const errorMsg = 'bcrypt compare error';

            bcrypt.compare.mockRejectedValue(new Error(errorMsg));

            // Act
            await expect(user.checkPassword(hpassword, password)).rejects.toThrow(errorMsg);

            // Assert
            expect(bcrypt.compare).toHaveBeenCalledWith(password, hpassword);
        });
    });

    // getUser tests
    describe('getUser', () => {
        it('should return a user object when the username exists', async () => {
            const expectedUser = { username, email: 'test@email.com', password: 'hashedPassword123' };

            mockDb.query.mockResolvedValue([[expectedUser], {}]);

            // Act
            const result = await user.getUser(username);

            // Assert
            expect(mockDb.query).toHaveBeenCalledWith("SELECT * FROM user WHERE username = ?", [username]);
            expect(result).toEqual(expectedUser);
        });

        it('should return undefined when the username does not exist', async () => {
            // Arrange
            const username = 'nonexistentUser';

            mockDb.query.mockResolvedValue([[], {}]);

            // Act
            const result = await user.getUser(username);

            // Assert
            expect(mockDb.query).toHaveBeenCalledWith("SELECT * FROM user WHERE username = ?", [username]);
            expect(result).toBeUndefined();
        });

        it('should throw an error when the query fails', async () => {
            // Arrange
            const username = 'testUser';
            const errorMsg = 'Database query error';

            mockDb.query.mockRejectedValue(new Error(errorMsg));

            // Act
            await expect(user.getUser(username)).rejects.toThrow(errorMsg);

            // Assert
            expect(mockDb.query).toHaveBeenCalledWith("SELECT * FROM user WHERE username = ?", [username]);
        });
    });

    // getUserByEmail tests
    describe('getUserByEmail', () => {
        it('should return a user when the email exists', async () => {
            // Arrange
            const email = 'test@test.com';
            const mockUser = { id: 1, username: 'test', email: email };

            mockDb.query.mockResolvedValue([[mockUser], {}]);

            // Act
            const result = await user.getUserByEmail(email);

            // Assert
            expect(mockDb.query).toHaveBeenCalledWith("SELECT * FROM user WHERE email = ?", [email]);
            expect(result).toEqual(mockUser);
        });

        it('should return undefined when the email does not exist', async () => {
            // Arrange
            const email = 'nonexistent@test.com';

            mockDb.query.mockResolvedValue([[], {}]);

            // Act
            const result = await user.getUserByEmail(email);

            // Assert
            expect(mockDb.query).toHaveBeenCalledWith("SELECT * FROM user WHERE email = ?", [email]);
            expect(result).toBeUndefined();
        });
    });

    // getUserProfile tests
    describe('getUserProfile', () => {
        it('should return a user profile when the user_id exists', async () => {
            // Arrange
            const id = 1;
            const mockProfile = {
                user_id: id,
                first_name: 'John',
                last_name: 'Doe',
                age: 30,
                gender: 'M',
                address: '123 Street',
                website: 'https://johndoe.com'
            };

            mockDb.query.mockResolvedValue([[mockProfile], {}]);

            // Act
            const result = await user.getUserProfile(id);

            // Assert
            expect(mockDb.query).toHaveBeenCalledWith("SELECT * FROM profile WHERE user_id = ?", [id]);
            expect(result).toEqual(mockProfile);
        });

        it('should return undefined when the user_id does not have a profile', async () => {
            // Arrange
            const id = 2;

            mockDb.query.mockResolvedValue([[], {}]);

            // Act
            const result = await user.getUserProfile(id);

            // Assert
            expect(mockDb.query).toHaveBeenCalledWith("SELECT * FROM profile WHERE user_id = ?", [id]);
            expect(result).toBeUndefined();
        });
    });

    // updateUserProfile tests
    describe('updateUserProfile', () => {
        const query = `
                INSERT INTO profile (user_id, first_name, last_name, age, gender, address, website)
                VALUES (?, ?, ?, ?, ?, ?, ?)
                ON DUPLICATE KEY UPDATE first_name = VALUES(first_name),
                                        last_name  = VALUES(last_name),
                                        age        = VALUES(age),
                                        gender     = VALUES(gender),
                                        address    = VALUES(address),
                                        website    = VALUES(website);
            `;


        it('should update an existing user profile and return true', async () => {
            mockDb.query.mockResolvedValue([{ affectedRows: 1 }, {}]);

            // Act
            const result = await user.updateUserProfile(profile);


            // Assert
            expect(mockDb.query).toHaveBeenCalledWith(query,
                [
                    profile.user_id,
                    profile.first_name,
                    profile.last_name,
                    profile.age,
                    profile.gender,
                    profile.address,
                    profile.website
                ]
            );
            expect(result).toBe(true);
        });

        it('should return false when no rows were affected', async () => {
            mockDb.query.mockResolvedValue([{ affectedRows: 0 }, {}]);
            // Act
            const result = await user.updateUserProfile(profile);

            // Assert
            expect(mockDb.query).toHaveBeenCalledWith(query,
                [
                    profile.user_id,
                    profile.first_name,
                    profile.last_name,
                    profile.age,
                    profile.gender,
                    profile.address,
                    profile.website
                ]
            );
            expect(result).toBe(false);
        });
    });

    // updateUserPassword tests
    describe('updateUserPassword', () => {
        it('should update the user password and return true', async () => {
            // Arrange
            const username = 'johnDoe';
            const password = 'newPassword';

            bcrypt.hash.mockResolvedValue('hashedNewPassword');
            mockDb.query.mockResolvedValue([{ affectedRows: 1 }, {}]);

            // Act
            const result = await user.updateUserPassword(username, password);

            // Assert
            expect(bcrypt.hash).toHaveBeenCalledWith(password.toString(), 10);
            expect(mockDb.query).toHaveBeenCalledWith(
                "UPDATE user SET password = ? WHERE username = ?",
                ['hashedNewPassword', username]
            );
            expect(result).toBe(true);
        });

        it('should return false when the user does not exist', async () => {
            // Arrange
            const username = 'nonexistentUser';
            const password = 'newPassword';

            bcrypt.hash.mockResolvedValue('hashedNewPassword');
            mockDb.query.mockResolvedValue([{ affectedRows: 0 }, {}]);

            // Act
            const result = await user.updateUserPassword(username, password);

            // Assert
            expect(bcrypt.hash).toHaveBeenCalledWith(password.toString(), 10);
            expect(mockDb.query).toHaveBeenCalledWith(
                "UPDATE user SET password = ? WHERE username = ?",
                ['hashedNewPassword', username]
            );
            expect(result).toBe(false);
        });
    });

    // deleteUser tests
    describe('deleteUser', () => {
        it('should delete the user and return true', async () => {
            // Arrange
            const username = 'johnDoe';

            mockDb.query.mockResolvedValue([{ affectedRows: 1 }, {}]);

            // Act
            const result = await user.deleteUser(username);

            // Assert
            expect(mockDb.query).toHaveBeenCalledWith(
                "DELETE FROM user WHERE username = ?",
                [username]
            );
            expect(result).toBe(true);
        });

        it('should return false when the user does not exist', async () => {
            // Arrange
            const username = 'nonexistentUser';

            mockDb.query.mockResolvedValue([{ affectedRows: 0 }, {}]);

            // Act
            const result = await user.deleteUser(username);

            // Assert
            expect(mockDb.query).toHaveBeenCalledWith(
                "DELETE FROM user WHERE username = ?",
                [username]
            );
            expect(result).toBe(false);
        });
    });
});
