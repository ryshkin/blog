const Post = require('../../models/post');
const { mocked } = require('jest-mock');

describe('Post', () => {
    let db, post;

    beforeEach(() => {
        // Create a mock database object
        db = {
            query: jest.fn(),
            close: jest.fn(),
        };
        post = new Post(db);
    });

    test('should add a post successfully', async () => {
        // Mock the database response
        db.query.mockResolvedValue([{ insertId: 1 }]);

        const postId = await post.add(1, 'Test Title', 'Test Description', 'Test Body');

        expect(postId).toEqual(1);
        // Verify that the query was called with the correct arguments
        expect(db.query).toHaveBeenCalledWith(
            'INSERT INTO post (user_id, title, description, body) VALUES (?, ?, ?, ?)',
            [1, 'Test Title', 'Test Description', 'Test Body']
        );
    });

    test('should update a post successfully', async () => {
        // Mock the database response
        db.query.mockResolvedValue([{ changedRows: 1 }]);

        const changedRows = await post.update(1, 1, 'Updated Title', 'Updated Description', 'Updated Body');

        expect(changedRows).toEqual(1);
        expect(db.query).toHaveBeenCalledWith(
            `
                UPDATE post
                SET title       = ?,
                    description = ?,
                    body        = ?
                WHERE id = ? and user_id =?;
            `,
            ['Updated Title', 'Updated Description', 'Updated Body', 1, 1]
        );
    });

    test('should search posts successfully', async () => {
        // Mock the database response
        db.query.mockResolvedValue([
            [
                {
                    id: 1,
                    title: 'Test Title',
                    description: 'Test Description',
                    body: 'Test Body',
                    user_id: 1,
                    created_at: '2023-04-25T10:00:00.000Z',
                    updated_at: '2023-04-25T10:00:00.000Z',
                    author: 'John Doe',
                },
            ],
        ]);

        const rows = await post.search();

        expect(rows).toHaveLength(1);
        expect(rows[0]).toMatchObject({
            id: 1,
            title: 'Test Title',
            description: 'Test Description',
            body: 'Test Body',
            user_id: 1,
            author: 'John Doe',
        });
        expect(db.query).toHaveBeenCalled();
    });

    test('should add a comment successfully', async () => {
        // Mock the database response
        db.query.mockResolvedValue([{ insertId: 1 }]);

        const commentId = await post.addComment(1, 1, 'Test Name', 'Test Message');

        expect(commentId).toEqual(1);
        expect(db.query).toHaveBeenCalledWith(
            'INSERT INTO comments (user_id, post_id, name, message) VALUES (?, ?, ?, ?)',
            [1, 1, 'Test Name', 'Test Message']
        );
    });

    test('should get a post successfully', async () => {
        // Mock the database response
        db.query.mockResolvedValue([
            [
                {
                    id: 1,
                    title: 'Test Title',
                    description: 'Test Description',
                    body: 'Test Body',
                    user_id: 1,
                    created_at: '2023-04-25T10:00:00.000Z',
                    updated_at: '2023-04-25T10:00:00.000Z',
                    author: 'John Doe',
                },
            ]
        ]);

        const postResult = await post.get(1);
        expect(postResult).toMatchObject({
            id: 1,
            title: 'Test Title',
            description: 'Test Description',
            body: 'Test Body',
            user_id: 1,
            author: 'John Doe',
        });
        const query = `
            SELECT post.*,  concat_ws(' ', profile.first_name, profile.last_name) as author
            FROM post
            LEFT JOIN profile on profile.user_id = post.user_id    
            WHERE post.id = ?
        `;
        expect(db.query).toHaveBeenCalledWith(query, [1]);
    });

    test('should get post comments successfully', async () => {
        // Mock the database response
        db.query.mockResolvedValue([
            [
                {
                    id: 1,
                    user_id: 1,
                    post_id: 1,
                    name: 'Test Name',
                    message: 'Test Message',
                    created_at: '2023-04-25T10:00:00.000Z',
                    updated_at: '2023-04-25T10:00:00.000Z',
                },
            ],
        ]);

        const comments = await post.getPostComments(1);

        expect(comments).toHaveLength(1);
        expect(comments[0]).toMatchObject({
            id: 1,
            user_id: 1,
            post_id: 1,
            name: 'Test Name',
            message: 'Test Message',
        });
        let query = `
            SELECT *
            FROM comments
            WHERE post_id = ?
            order by created_at desc
        `;
        expect(db.query).toHaveBeenCalledWith(query, [1]);
    });

    test('should throw an error if the database query fails', async () => {
        // Arrange
        const mockDb = {
            query: jest.fn().mockRejectedValue(new Error('DB error')),
        };
        const post = new Post(mockDb);

        // Act and Assert
        await expect(post.search()).rejects.toThrowError('Error fetching posts: DB error');
    });

    test('should throw an error if the database query fails', async () => {
        // Arrange
        const mockDb = {
            query: jest.fn().mockRejectedValue(new Error('DB error')),
        };
        const post = new Post(mockDb);
        const postId = 1;

        // Act and Assert
        await expect(post.get(postId)).rejects.toThrowError('Error fetching post: DB error');
    });

    test('should throw an error if the database query fails', async () => {
        // Arrange
        const mockDb = {
            query: jest.fn().mockRejectedValue(new Error('DB error')),
        };
        const post = new Post(mockDb);
        const postId = 1;
        const limit = 10;

        // Act and Assert
        await expect(post.getPostComments(postId, limit)).rejects.toThrowError('Error fetching post: DB error');
    });


})
