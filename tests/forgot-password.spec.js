const request = require('supertest');
const app = require('../app');

const mailer = app.get('mailer');
const db = app.get('db');
const user = app.get('user');


describe('POST /api/user/forgot-password', () => {
    beforeAll(async () => {
        await user.deleteUser('found');
        await user.addUser('found', 'found@example.com', 'valid');
    })

    it('should return 400 if email is missing', async () => {
        const res = await request(app).post('/api/user/forgot-password').send({});
        expect(res.status).toEqual(400);
        expect(res.body).toHaveProperty('error', 'Email is required');
    });

    it('should return 404 if email is not found', async () => {
        const res = await request(app).post('/api/user/forgot-password').send({ email: 'notfound@example.com' });
        expect(res.status).toEqual(404);
        expect(res.body).toHaveProperty('error', 'No account with that email address exists.');
    });

    it('should return 200 and send an email if email is found', async () => {
        jest.spyOn(mailer, 'sendMail').mockImplementation(() => true);
        const res = await request(app).post('/api/user/forgot-password').send({ email: 'found@example.com' });
        expect(res.status).toEqual(200);
        expect(res.body).toHaveProperty('message', 'Password reset email sent');
        expect(mailer.sendMail).toHaveBeenCalledTimes(1);
    });

    afterAll(async () => {
        await user.deleteUser('found');
        await db.end();
    })
});
