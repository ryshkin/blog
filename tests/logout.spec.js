const request = require('supertest');
const app = require('../app');

const db = app.get('db');
const user = app.get('user');

describe('GET /api/user/logout', () => {
    beforeAll(async () => {
        await user.deleteUser('valid');
        await user.addUser('valid', 'valid@test.net', 'valid');
    })

    it('should return 401 if no token is provided', async () => {
        const res = await request(app).get('/api/user/logout');
        expect(res.status).toEqual(401);
        expect(res.body).toHaveProperty('error', 'Authorization header is required');
    });

    it('should return 401 if an invalid token is provided', async () => {
        const res = await request(app).get('/api/user/logout').set('Authorization', 'Bearer invalid_token');
        expect(res.status).toEqual(401);
        expect(res.body).toHaveProperty('error', 'Invalid token');
    });

    it('should return 200 if a valid token is provided', async () => {
        // First, obtain a valid token by logging in
        const loginRes = await request(app).post('/api/user/login').send({ username: 'valid', password: 'valid' });
        const token = loginRes.body.token;

        // Then, use the token to log out
        const res = await request(app).get('/api/user/logout').set('Authorization', `Bearer ${token}`);
        expect(res.status).toEqual(200);
        expect(res.body).toHaveProperty('message', 'Logout successful');
    });

    afterAll(async () => {
        await user.deleteUser('valid');
        await db.end();
    })
});
