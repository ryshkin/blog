const request = require('supertest');
const app = require('../app');

const db = app.get('db');
const user = app.get('user');
const post = app.get('post');

let authToken;
let userId;
let postId;

beforeAll(async () => {
    await user.deleteUser('valid');
    userId = await user.addUser('valid', 'valid@test.net', 'valid');
    postId = await post.add(userId, 'Test Title', 'Test Description', 'Test Body');
    // Login to get an authentication token
    const loginRes = await request(app).post('/api/user/login').send({ username: 'valid', password: 'valid' });
    authToken = loginRes.body.token;
});

afterAll(async () => {
    await user.deleteUser('valid');
    await db.end();
});


describe('POST /api/post/add/comment', () => {
    test('should add a comment successfully', async () => {
        const res = await request(app)
            .post('/api/post/add/comment')
            .send({
                postId,
                name: 'Test Name',
                message: 'Test message',
            }).set('Authorization', `Bearer ${authToken}`);

        expect(res.statusCode).toEqual(201);
        expect(res.body).toHaveProperty('message', 'Comment added to the Post successfully!');
        expect(res.body).toHaveProperty('commentId');
    });

    test('should return an error for missing name', async () => {
        const res = await request(app)
            .post('/api/post/add/comment')
            .send({
                postId,
                message: 'Test message',
            }).set('Authorization', `Bearer ${authToken}`);

        expect(res.statusCode).toEqual(400);
        expect(res.body).toHaveProperty('error', 'The name is required and cannot be empty');
        expect(res.body).toHaveProperty('input_field', 'name');
    });

    test('should return an error for missing message', async () => {
        // Replace the `postId` variable with an existing post ID in your test database
        const postId = 1;
        const res = await request(app)
            .post('/api/post/add/comment')
            .send({
                postId,
                name: 'Test Name',
            }).set('Authorization', `Bearer ${authToken}`);

        expect(res.statusCode).toEqual(400);
        expect(res.body).toHaveProperty('error', 'The message is required and cannot be empty');
        expect(res.body).toHaveProperty('input_field', 'message');
    });

    test('should return an error for non-existing user', async () => {
        // TODO: You'll need to mock the `user.getUser()` function to return null for this test
        jest.spyOn(user, 'getUser').mockImplementation(() => null);
        const res = await request(app)
            .post('/api/post/add/comment')
            .send({
                postId,
                name: 'Test Name',
                message: 'Test message',
            }).set('Authorization', `Bearer ${authToken}`);

        expect(res.statusCode).toEqual(404);
        // You might need to catch the thrown error in your route handler and send it as a response
        expect(res.body).toHaveProperty('error', 'User not found');
    });
});
