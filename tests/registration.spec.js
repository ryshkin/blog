const request = require('supertest');
const app = require('../app');

const db = app.get('db');
const user = app.get('user');

describe('POST /api/user/register', () => {
    beforeAll(async () => {
        await user.deleteUser('testuser');
    })

    it('should return 400 if username is empty', async () => {
        const res = await request(app)
            .post('/api/user/register')
            .send({
                email: 'test@example.com',
                password: 'password',
                confirm_password: 'password',
            });
        expect(res.statusCode).toEqual(400);
        expect(res.body).toHaveProperty('error', 'The username is required and cannot be empty');
        expect(res.body).toHaveProperty('input_field', 'username');
    });

    it('should return 400 if email is not valid', async () => {
        const res = await request(app)
            .post('/api/user/register')
            .send({
                username: 'testuser',
                email: 'invalid-email',
                password: 'password',
                confirm_password: 'password',
            });
        expect(res.statusCode).toEqual(400);
        expect(res.body).toHaveProperty('error', 'The email address is not valid');
        expect(res.body).toHaveProperty('input_field', 'email');
    });

    it('should return 400 if password and confirm_password do not match', async () => {
        const res = await request(app)
            .post('/api/user/register')
            .send({
                username: 'testuser',
                email: 'test@example.com',
                password: 'password1',
                confirm_password: 'password2',
            });
        expect(res.statusCode).toEqual(400);
        expect(res.body).toHaveProperty('error', 'The password and its confirm are not the same');
        expect(res.body).toHaveProperty('input_field', 'confirm_password');
    });

    it('should return 201 if registration is successful', async () => {
        const res = await request(app)
            .post('/api/user/register')
            .send({
                username: 'testuser',
                email: 'test@example.com',
                password: 'password',
                confirm_password: 'password',
            });
        expect(res.statusCode).toEqual(201);
        expect(res.body).toHaveProperty('message', 'Congrats! Your registration has been successful.');
    });

    afterAll(async () => {
        await user.deleteUser('testuser');
        await db.end();
    })
});

