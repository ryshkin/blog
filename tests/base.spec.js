const request = require('supertest');
const app = require('../app');

const db = app.get('db');

describe('GET /api/ping', () => {
    it('should return 200 OK', async () => {
        const res = await request(app).get('/api/ping');
        expect(res.statusCode).toEqual(200);
    });

    it('should return JSON', async () => {
        const res = await request(app).get('/api/ping');
        expect(res.headers['content-type']).toEqual(expect.stringContaining('json'));
    });

    it('should return { server: "OK", database: "OK", time: <current time> }', async () => {
        const res = await request(app).get('/api/ping');
        expect(res.body.server).toEqual('OK');
        expect(res.body.database).toEqual('OK');
        expect(res.body.time).toBeDefined();
    });

    afterAll(async () => {
        await db.end();
    })
});


