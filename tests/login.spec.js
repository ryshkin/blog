const request = require('supertest');
const app = require('../app');

const db = app.get('db');
const user = app.get('user');

describe('POST /api/user/login', () => {
    beforeAll(async () => {
        await user.deleteUser('valid');
        await user.addUser('valid', 'valid@test.net', 'valid');
    })

    it('should return 400 if username or password is missing', async () => {
        const res = await request(app).post('/api/user/login').send({});
        expect(res.status).toEqual(400);
        expect(res.body).toHaveProperty('error', 'Username and password are required');
    });

    it('should return 401 if username or password is invalid', async () => {
        const res = await request(app).post('/api/user/login').send({ username: 'invalid', password: 'invalid' });
        expect(res.status).toEqual(401);
        expect(res.body).toHaveProperty('error', 'Invalid username/password, Try again!');
    });

    it('should return 200 and a token if username and password are valid', async () => {
        const res = await request(app).post('/api/user/login').send({ username: 'valid', password: 'valid' });
        expect(res.status).toEqual(200);
        expect(res.body).toHaveProperty('message', 'Login successful');
        expect(res.body).toHaveProperty('token');
    });

    afterAll(async () => {
        await user.deleteUser('valid');
        await db.end();
    })
});
