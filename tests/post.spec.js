const request = require('supertest');
const app = require('../app');

const db = app.get('db');
const user = app.get('user');
const post = app.get('post');

let authToken;
let userId;

beforeAll(async () => {
    await user.deleteUser('valid');
    userId = await user.addUser('valid', 'valid@test.net', 'valid');
    // Login to get an authentication token
    const loginRes = await request(app).post('/api/user/login').send({ username: 'valid', password: 'valid' });
    authToken = loginRes.body.token;
});

afterAll(async () => {
    await user.deleteUser('valid');
    await db.end();
});



describe('POST /api/post/add', () => {

    test('Add a new blog post with valid data', async () => {
        const validPost = {
            title: 'Test Title',
            description: 'Test Description',
            body: 'Test Body',
        };

        const response = await request(app)
            .post('/api/post/add')
            .send(validPost).set('Authorization', `Bearer ${authToken}`);

        expect(response.status).toBe(201);
        expect(response.body).toHaveProperty('message', 'Blog Post posted successfully!');
        expect(response.body).toHaveProperty('postId');
    });

    test('Fail to add a new blog post with missing title', async () => {
        const invalidPost = {
            description: 'Test Description',
            body: 'Test Body',
        };

        const response = await request(app)
            .post('/api/post/add')
            .send(invalidPost).set('Authorization', `Bearer ${authToken}`);;

        expect(response.status).toBe(400);
        expect(response.body).toHaveProperty('error', 'The title is required and cannot be empty');
        expect(response.body).toHaveProperty('input_field', 'title');
    });
});

describe('PUT /api/post/update/:postId', () => {
    let postId;

    beforeAll(async () => {
        postId = await post.add(userId, 'Test Title', 'Test Description', 'Test Body');
    });

    test('should update a post successfully', async () => {
        const response = await request(app)
            .put(`/api/post/update/${postId}`)
            .set('Authorization', `Bearer ${authToken}`)
            .send({
                title: 'Updated Title',
                description: 'Updated Description',
                body: 'Updated Body'
            });

        expect(response.status).toBe(200);
        expect(response.body.message).toBe('Blog Post updated successfully!');
        expect(response.body.postId).toBe(postId.toString());
    });

    test('should return an error when title is missing', async () => {
        const response = await request(app)
            .put(`/api/post/update/${postId}`)
            .set('Authorization', `Bearer ${authToken}`)
            .send({
                description: 'Updated Description',
                body: 'Updated Body'
            });

        expect(response.status).toBe(400);
        expect(response.body.error).toBe('The title is required and cannot be empty');
        expect(response.body.input_field).toBe('title');
    });
});

describe('GET /api/post/search', () => {
    beforeAll(async () => {
        for (let i = 1; i <= 15; i++) {
            await post.add(userId, 'test - ' + i, 'Test Description - ' + i, 'Test Body - ' + i);
        }
    });

    test('should return posts with default pagination', async () => {
        const res = await request(app).get('/api/post/search');
        expect(res.statusCode).toEqual(200);
        expect(res.body.length).toBeLessThanOrEqual(10);
    });

    test('should return posts with custom pagination', async () => {
        const res = await request(app).get('/api/post/search?page=2&limit=5');
        expect(res.statusCode).toEqual(200);
        expect(res.body.length).toBeLessThanOrEqual(5);
    });

    test('should return posts with search', async () => {
        const searchTerm = 'test';
        const res = await request(app).get(`/api/post/search?search=${searchTerm}`);
        expect(res.statusCode).toEqual(200);
        expect(res.body).toEqual(
            expect.arrayContaining([
                expect.objectContaining({
                    title: expect.stringContaining(searchTerm),
                }),
            ])
        );
    });

    test('should return empty array if no posts match the search term', async () => {
        const searchTerm = 'non_existent_search_term';
        const res = await request(app).get(`/api/post/search?search=${searchTerm}`);
        expect(res.statusCode).toEqual(200);
        expect(res.body).toEqual([]);
    });
});

describe('GET /api/post/post/:postId', () => {
    let postId;

    beforeEach(async () => {
        postId = await post.add(userId, 'Test Title', 'Test Description', 'Test Body');
    });

    it('should get a blog post by ID', async () => {
        const res = await request(app)
            .get(`/api/post/post/${postId}`)
            .set('Authorization', `Bearer ${authToken}`);

        expect(res.status).toEqual(200);
        expect(res.body).toHaveProperty('id');
        expect(res.body.id).toBe(postId);
    });


    it('should return a 404 if the post is not found', async () => {
        const res = await request(app)
            .get(`/api/post/post/777`)
            .set('Authorization', `Bearer ${authToken}`);

        expect(res.status).toEqual(404);
        expect(res.body).toHaveProperty('error');
        expect(res.body.error).toBe('Post not found or you do not have permission to get it');
    });

});

describe('GET /api/post/comments/:postId', () => {
    let postId;

    beforeEach(async () => {
        postId = await post.add(userId, 'Test Title', 'Test Description', 'Test Body');
        for (let i = 1; i <= 3; i++) {
            await post.addComment(userId, postId, 'test name- ' + i, 'Test message - ' + i);
        }
    });


    it('should get blog post comments by post ID', async () => {
        const res = await request(app)
            .get(`/api/post/comments/${postId}`)
            .set('Authorization', `Bearer ${authToken}`);

        expect(res.status).toEqual(200);
        expect(res.body).toBeInstanceOf(Array);
        expect(res.body.length).toEqual(3);

        expect(res.body[0]).toHaveProperty('id');
        expect(res.body[0]).toHaveProperty('user_id');
        expect(res.body[0]).toHaveProperty('post_id');
        expect(res.body[0]).toHaveProperty('name');
        expect(res.body[0]).toHaveProperty('message');
    });

    it('should return a zero if the post ID is invalid or no comments', async () => {
        const res = await request(app)
            .get(`/api/post/comments/777`)
            .set('Authorization', `Bearer ${authToken}`);

        expect(res.status).toEqual(200);
        expect(res.body).toBeInstanceOf(Array);
        expect(res.body.length).toEqual(0);
    });
});


