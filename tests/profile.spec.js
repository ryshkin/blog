const request = require('supertest');
const app = require('../app');

const db = app.get('db');
const user = app.get('user');


describe('User Profile', () => {
    let authToken;

    beforeAll(async () => {
        await user.deleteUser('valid');
        await user.addUser('valid', 'valid@test.net', 'valid');
        // Login to get an authentication token
        const loginRes = await request(app).post('/api/user/login').send({ username: 'valid', password: 'valid' });
        authToken = loginRes.body.token;
    });

    it('should get the user profile', async () => {
        const res = await request(app).get('/api/user/profile').set('Authorization', `Bearer ${authToken}`);
        expect(res.status).toBe(200);
        expect(res.body).toHaveProperty('username');
        expect(res.body).toHaveProperty('email');
        expect(res.body).toHaveProperty('first_name');
        expect(res.body).toHaveProperty('last_name');
        expect(res.body).toHaveProperty('age');
        expect(res.body).toHaveProperty('gender');
        expect(res.body).toHaveProperty('address');
        expect(res.body).toHaveProperty('website');
    });

    it('should return an error if the user is not authenticated', async () => {
        const res = await request(app).get('/api/user/profile');
        expect(res.status).toBe(401);
    });

    it('should update the user profile', async () => {
        const updatedProfile = {
            first_name: 'John',
            last_name: 'Doe',
            age: 30,
            gender: 'Male',
            address: '123 Main St',
            website: 'https://example.com',
        };

        const res = await request(app)
            .post('/api/user/profile')
            .set('Authorization', `Bearer ${authToken}`)
            .send(updatedProfile);

        expect(res.status).toBe(200);
        expect(res.body).toMatchObject(updatedProfile);
    });

    it('should return an error if the user is not authenticated', async () => {
        const updatedProfile = {
            first_name: 'John',
            last_name: 'Doe',
            age: 30,
            gender: 'Male',
            address: '123 Main St',
            website: 'https://example.com',
        };

        const res = await request(app).post('/api/user/profile').send(updatedProfile);
        expect(res.status).toBe(401);
    });

    afterAll(async () => {
        await user.deleteUser('valid');
        await db.end();
    })
});
