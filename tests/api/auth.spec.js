const { authenticate, logout, tokenBlacklist } = require('../../api/auth');
const jwt = require('jsonwebtoken');
const httpMocks = require('node-mocks-http');

describe('auth middleware', () => {
    let req, res, next;
    const secretKey = 'test_secret_key';
    const config = { secretKey };
    const token = jwt.sign({ username: 'testUser' }, secretKey, { expiresIn: '1h' });

    beforeEach(() => {
        req = httpMocks.createRequest();
        res = httpMocks.createResponse();
        next = jest.fn();
        req.app = {
            get: () => config,
        };
    });

    it('should return a 401 error if the Authorization header is missing', () => {
        authenticate(req, res, next);

        expect(res.statusCode).toBe(401);
        expect(res._getJSONData()).toEqual({ error: 'Authorization header is required' });
        expect(next).not.toHaveBeenCalled();
    });

    it('should return a 401 error if the token is blacklisted', () => {
        req.headers.authorization = `Bearer ${token}`;
        logout(token);
        authenticate(req, res, next);

        expect(res.statusCode).toBe(401);
        expect(res._getJSONData()).toEqual({ error: 'Token is blacklisted' });
        expect(next).not.toHaveBeenCalled();
    });

    it('should return a 401 error if the token is expired', () => {
        const expiredToken = jwt.sign({ username: 'testUser' }, secretKey, { expiresIn: '-1h' });
        req.headers.authorization = `Bearer ${expiredToken}`;

        authenticate(req, res, next);

        expect(res.statusCode).toBe(401);
        expect(res._getJSONData()).toEqual({ error: 'Token expired' });
        expect(next).not.toHaveBeenCalled();
    });

    it('should return a 401 error if the token is invalid', () => {
        const invalidToken = jwt.sign({ username: 'testUser' }, 'wrong_secret_key');
        req.headers.authorization = `Bearer ${invalidToken}`;

        authenticate(req, res, next);

        expect(res.statusCode).toBe(401);
        expect(res._getJSONData()).toEqual({ error: 'Invalid token' });
        expect(next).not.toHaveBeenCalled();
    });

    it('should return a 401 status and error message if the Authorization header is missing', async () => {
        res.status = jest.fn().mockReturnValue(res);
        res.json = jest.fn().mockReturnValue(res);

        delete req.headers.authorization;

        await authenticate(req, res, next);

        expect(res.status).toHaveBeenCalledWith(401);
        expect(res.json).toHaveBeenCalledWith({ error: 'Authorization header is required' });
        expect(next).not.toHaveBeenCalled();
    });

    it('should return a 401 status and error message if the token is blacklisted', async () => {
        res.status = jest.fn().mockReturnValue(res);
        res.json = jest.fn().mockReturnValue(res);

        tokenBlacklist.add(token);
        req.headers.authorization = `Bearer ${token}`;

        await authenticate(req, res, next);

        expect(res.status).toHaveBeenCalledWith(401);
        expect(res.json).toHaveBeenCalledWith({ error: 'Token is blacklisted' });
        expect(next).not.toHaveBeenCalled();
    });

    it('should return a 401 status and error message if the token is expired', async () => {
        res.status = jest.fn().mockReturnValue(res);
        res.json = jest.fn().mockReturnValue(res);

        const expiredToken = jwt.sign({ username: 'testUser' }, config.secretKey, { expiresIn: '-1s' });
        req.headers.authorization = `Bearer ${expiredToken}`;

        await authenticate(req, res, next);

        expect(res.status).toHaveBeenCalledWith(401);
        expect(res.json).toHaveBeenCalledWith({ error: 'Token expired' });
        expect(next).not.toHaveBeenCalled();
    });

    it('should return a 401 status and error message if the token is invalid', async () => {
        res.status = jest.fn().mockReturnValue(res);
        res.json = jest.fn().mockReturnValue(res);

        const invalidToken = jwt.sign({ username: 'testUser' }, 'wrongSecretKey', { expiresIn: '1h' });
        req.headers.authorization = `Bearer ${invalidToken}`;

        await authenticate(req, res, next);

        expect(res.status).toHaveBeenCalledWith(401);
        expect(res.json).toHaveBeenCalledWith({ error: 'Invalid token' });
        expect(next).not.toHaveBeenCalled();
    });

});
